package com.smallisdigital.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.smallisdigital.app.R;
import com.smallisdigital.app.activity.CreateRequestActivity;

public class FinishRequestFragment extends Fragment {
    private OnFragmentInteractionListener mListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_finish_request, container, false);

        CreateRequestActivity parent = ((CreateRequestActivity) getActivity());

        TextInputLayout tilAddress = rootView.findViewById(R.id.til_address);
        TextInputLayout tilDescription = rootView.findViewById(R.id.til_description);
        Button picture = rootView.findViewById(R.id.picture);
        Button send = rootView.findViewById(R.id.send);

        ImageView imageView = rootView.findViewById(R.id.image_view);

        if(parent != null) {
            parent.setAddress(tilAddress);
            parent.setDescription(tilDescription);
            parent.setImageView(imageView);
        }

        picture.setOnClickListener(view -> onButtonPressed(picture));
        send.setOnClickListener(view -> onButtonPressed(send));

        return rootView;
    }

    public void onButtonPressed(View view) {
        if(mListener != null)
            mListener.onFragmentFinishInteraction(view);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if(context instanceof OnFragmentInteractionListener)
            mListener = (OnFragmentInteractionListener) context;
        else
            throw new RuntimeException(context + " must be implement OnFragmentInteractionListener");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentFinishInteraction(View view);
    }
}
