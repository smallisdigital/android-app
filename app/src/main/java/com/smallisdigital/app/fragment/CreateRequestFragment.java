package com.smallisdigital.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.smallisdigital.app.R;
import com.smallisdigital.app.asynctask.HttpRequest;
import com.smallisdigital.app.common.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

public class CreateRequestFragment extends Fragment {
    private static final Logger logger = Logger.getLogger(CreateRequestFragment.class.getName());

    private final Button[] button = new Button[6];
    private int step = 0;

    private OnFragmentInteractionListener mListener;

    private HttpRequest request;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_create_request, container, false);

        request = new HttpRequest(getContext());

        button[0] = rootView.findViewById(R.id.button1);
        button[1] = rootView.findViewById(R.id.button2);
        button[2] = rootView.findViewById(R.id.button3);
        button[3] = rootView.findViewById(R.id.button4);
        button[4] = rootView.findViewById(R.id.button5);
        button[5] = rootView.findViewById(R.id.button6);

        for(int i = 0 ; i < 6 ; i++) {
            int finalI = i;
            button[i].setOnClickListener(view -> onButtonPressed(button[finalI]));
        }

        getCategory();

        return rootView;
    }

    public void onButtonPressed(View view) {
        if(view instanceof Button vButton) {
            if(mListener != null)
                mListener.onFragmentCreateInteraction(view, step);

            if(step == 0)
                getsubCategory(Integer.parseInt((String) vButton.getContentDescription()));
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if(context instanceof OnFragmentInteractionListener)
            mListener = (OnFragmentInteractionListener) context;
        else
            throw new RuntimeException(context + " must be implement OnFragmentInteractionListener");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentCreateInteraction(View view, int step);
    }

    public void getCategory() {
        String hostname = "/api/category/list";

        JSONObject result = request.get(hostname, "");

        if(result != null) {
            try {
                if((result.getBoolean("success"))) {
                    JSONArray categories = result.getJSONArray("category");

                    int i;
                    int l;

                    for(i = 0, l = categories.length() ; i < l && i < Constants.CATEGORY_SIZE_MAX ; i++) {
                        JSONObject category = categories.getJSONObject(i);

                        int id = category.getInt("id");
                        String name = category.getString("name");

                        button[i].setText(name);
                        button[i].setContentDescription(Integer.toString(id));
                    }

                    for( ; i < Constants.CATEGORY_SIZE_MAX ; i++)
                        button[i].setVisibility(View.GONE);
                }
            } catch(JSONException e) {
                logger.log(Level.SEVERE, e.getMessage());
            }
        }
    }

    public void getsubCategory(int categoryId) {
        Context context = getContext();

        String hostname = "/api/category/sub/list";

        JSONObject result = request.get(hostname, "category=" + categoryId);

        if(result != null) {
            try {
                if((result.getBoolean("success"))) {
                    JSONArray categories = result.getJSONArray("subCategories");

                    int i;
                    int l = categories.length();

                    if(l > 0) {
                        for (i = 0 ; i < l && i < Constants.SUBCATEGORY_SIZE_MAX ; i++) {
                            JSONObject category = categories.getJSONObject(i);

                            int id = category.getInt("id");
                            String name = category.getString("name");

                            button[i].setText(name);
                            button[i].setContentDescription(Integer.toString(id));
                        }

                        for ( ; i < Constants.SUBCATEGORY_SIZE_MAX ; i++)
                            button[i].setVisibility(View.GONE);

                        step++;
                    } else {
                        if(context != null)
                            Toast.makeText(context, context.getResources().getString(R.string.request_no_subcategory), Toast.LENGTH_SHORT).show();
                    }
                }
            } catch(JSONException e) {
                logger.log(Level.SEVERE, e.getMessage());
            }
        }
    }

    public int getStep() {
        return step;
    }
}
