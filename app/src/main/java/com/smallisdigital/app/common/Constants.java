package com.smallisdigital.app.common;

public class Constants {

    private Constants() {
        throw new IllegalStateException("Constants class");
    }

    public static final Integer CATEGORY_SIZE_MAX = 6;
    public static final Integer SUBCATEGORY_SIZE_MAX = 6;

    public static final Integer REQUEST_TODO = 1;
    public static final Integer REQUEST_RUNNING = 2;
    public static final Integer REQUEST_COMPLETED = 3;

    public static final String INTENT_STATUS = "status";
    public static final String INTENT_PRIORITY = "priority";
    public static final String INTENT_DELAY = "delay";
    public static final String INTENT_CONTRIBUTION = "contribution";
    public static final String INTENT_FIRSTNAME = "firstname";
    public static final String INTENT_LASTNAME = "lastname";
    public static final String INTENT_EMAIL = "email";
    public static final String INTENT_LEVEL = "level";
    public static final String INTENT_PHONE = "phone";
    public static final String INTENT_BIRTHDAY = "birthday";
    public static final String INTENT_ADDRESS = "address";
    public static final String INTENT_PASSWORD = "password";
    public static final String INTENT_REPASSWORD = "repassword";
    public static final String INTENT_CATEGORY_ID = "categoryId";
    public static final String INTENT_SUB_CATEGORY_ID = "subCategoryId";
    public static final String INTENT_CATEGORY_NAME = "categoryName";
    public static final String INTENT_SUB_CATEGORY_NAME = "subCategoryName";

}
