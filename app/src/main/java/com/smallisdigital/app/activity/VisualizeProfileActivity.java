package com.smallisdigital.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.smallisdigital.app.R;
import com.smallisdigital.app.adapter.VisualizeProfileAdapter;
import com.smallisdigital.app.asynctask.JsonObjectAsyncTask;
import com.smallisdigital.app.session.SessionManager;

public class VisualizeProfileActivity extends AppCompatActivity {

    private JsonObjectAsyncTask asyncTask = null;

    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_visualize_profile);

        /* Session */
        session = new SessionManager(this);

        /* Toolbar */
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /* Bottom navigation bar */
        BottomNavigationView bottomNavigationView = findViewById(R.id.nav_view);
        bottomNavigationView.setOnItemSelectedListener(navigationItemSelectedListener);

        final RecyclerView wrapper = findViewById(R.id.view);
        wrapper.setLayoutManager(new LinearLayoutManager(this));
        VisualizeProfileAdapter adapter = new VisualizeProfileAdapter(this);
        wrapper.setAdapter(adapter);

        /* Visibility gone when loading */
        wrapper.setVisibility(View.GONE);

        String hostname = "/api/user/";
        String params = "userId=" + session.getId();

        asyncTask = new JsonObjectAsyncTask(this, adapter);
        asyncTask.execute(hostname, params);

        /* Chargement de la page */
        final ProgressBar progress = findViewById(R.id.progress);
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                progress.setVisibility(View.GONE);
                wrapper.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(asyncTask !=  null)
            asyncTask.cancel(true);
    }

    private final BottomNavigationView.OnItemSelectedListener navigationItemSelectedListener = item -> {
        Intent intent;

        if (item.getItemId() == R.id.navigation_home) {
            if(session.isTaff())
                intent = new Intent(getApplicationContext(), HomeStaffActivity.class);
            else
                intent = new Intent(getApplicationContext(), HomeUserActivity.class);
            startActivity(intent);

            return true;
        }

        return false;
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        if(item.getItemId() == R.id.logout) {
            session.logout();
            intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finishAffinity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
