package com.smallisdigital.app.activity;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.highsoft.highcharts.common.HIColor;
import com.highsoft.highcharts.common.hichartsclasses.HIChart;
import com.highsoft.highcharts.common.hichartsclasses.HIColumn;
import com.highsoft.highcharts.common.hichartsclasses.HICredits;
import com.highsoft.highcharts.common.hichartsclasses.HIExporting;
import com.highsoft.highcharts.common.hichartsclasses.HILegend;
import com.highsoft.highcharts.common.hichartsclasses.HIOptions;
import com.highsoft.highcharts.common.hichartsclasses.HITitle;
import com.highsoft.highcharts.core.HIChartView;
import com.smallisdigital.app.R;
import com.smallisdigital.app.asynctask.HttpRequest;
import com.smallisdigital.app.common.Constants;
import com.smallisdigital.app.session.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;

public class HomeStaffActivity extends AppCompatActivity {

    private SessionManager session;

    private HIChartView chartView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home_staff);

        /* Session */
        session = new SessionManager(this);

        /* Toolbar */
        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        /* Bottom navigation bar */
        BottomNavigationView bottomNavigationView = findViewById(R.id.nav_view);
        bottomNavigationView.setOnItemSelectedListener(navigationItemSelectedListener);

        Button button1 = findViewById(R.id.button1);
        Button button2 = findViewById(R.id.button2);
        Button button3 = findViewById(R.id.button3);
        Button button4 = findViewById(R.id.button4);

        Button statisticDay = findViewById(R.id.statistic_day);
        Button statisticMonth = findViewById(R.id.statistic_month);

        chartView = findViewById(R.id.chart);

        HttpRequest request = new HttpRequest(this);

        String hostname = "/api/statistic/request";

        /* Button size */
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        button2.setWidth((int) (size.x / 2.3));
        button3.setWidth((int) (size.x / 2.3));

        if(session.isTaff()) {
            button1.setText(R.string.home_staff_button1);
            button2.setText(R.string.home_staff_button2);
            button3.setText(R.string.home_staff_button3);
            button4.setText(R.string.home_staff_button4);
        } else {
            button1.setVisibility(View.GONE);

            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) button2.getLayoutParams();
            layoutParams2.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            button2.setLayoutParams(layoutParams2);

            RelativeLayout.LayoutParams layoutParams3 = (RelativeLayout.LayoutParams) button3.getLayoutParams();
            layoutParams3.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            button3.setLayoutParams(layoutParams3);
        }

        button1.setOnClickListener(view -> {
            Intent intent;

            if(session.isTaff()) {
                intent = new Intent(this, RequestTaskActivity.class);
                intent.putExtra(Constants.INTENT_STATUS, Constants.REQUEST_TODO);
            } else
                intent = new Intent(this, ModifyProfileActivity.class);

            startActivity(intent);
        });

        button2.setOnClickListener(view -> {
            Intent intent;

            if(session.isTaff()) {
                intent = new Intent(this, RequestTaskActivity.class);
                intent.putExtra(Constants.INTENT_STATUS, Constants.REQUEST_RUNNING);
            } else
                intent = new Intent(this, CreateRequestActivity.class);

            startActivity(intent);
        });

        button3.setOnClickListener(view -> {
            Intent intent;

            if(session.isTaff()) {
                intent = new Intent(this, RequestTaskActivity.class);
                intent.putExtra(Constants.INTENT_STATUS, Constants.REQUEST_COMPLETED);
            } else
                intent = new Intent(this, VisualizeRequestActivity.class);

            startActivity(intent);
        });

        button4.setOnClickListener(view -> {
            Intent intent;

            if(session.isTaff())
                intent = new Intent(this, VisualizeSurveyActivity.class);
            else
                intent = new Intent(this, NoteSurveyActivity.class);

            startActivity(intent);
        });

        statisticDay.setOnClickListener(view -> {
            ArrayList<Integer> list = new ArrayList<>();

            String params = "period=day";

            try {
                JSONArray json = request.get_array(hostname, params);

                if(json != null) {
                    for (int i = 0, l = json.length() ; i < l ; i++)
                        list.add(json.getInt(i));
                }

                displayChart(list);
            } catch (JSONException ignored) {}
        });

        statisticMonth.setOnClickListener(view -> {
            ArrayList<Integer> list = new ArrayList<>();

            String params = "period=month";

            try {
                JSONArray json = request.get_array(hostname, params);

                if(json != null) {
                    for (int i = 0, l = json.length() ; i < l ; i++)
                        list.add(json.getInt(i));
                }

                displayChart(list);
            } catch (JSONException ignored) {}
        });

        ArrayList<Integer> list = new ArrayList<>();

        String params = "period=day";

        try {
            JSONArray json = request.get_array(hostname, params);

            if(json != null) {
                for (int i = 0, l = json.length() ; i < l ; i++)
                    list.add(json.getInt(i));
            }

            displayChart(list);

        } catch (JSONException ignored) {}
    }

    private final BottomNavigationView.OnItemSelectedListener navigationItemSelectedListener = item -> {
        Intent intent;

        if(item.getItemId() == R.id.navigation_profile) {
            intent = new Intent(this, VisualizeProfileActivity.class);
            startActivity(intent);

            return true;
        }

        return false;
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.top_nav_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        if(item.getItemId() == R.id.menu_planning) {
            intent = new Intent(getApplicationContext(), PlanningActivity.class);
            startActivity(intent);

            return true;
        } else if(item.getItemId() == R.id.logout) {
            session.logout();
            intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finishAffinity();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void displayChart(ArrayList<Integer> list) {
        HIOptions options = new HIOptions();

        HIChart chart = new HIChart();
        chart.setType("column");
        chart.setBackgroundColor(HIColor.initWithHexValue("EFEFEF"));
        options.setChart(chart);

        HITitle title = new HITitle();
        title.setText("");
        options.setTitle(title);

        HILegend legend = new HILegend();
        legend.setEnabled(false);
        options.setLegend(legend);

        HIExporting exporting = new HIExporting();
        exporting.setEnabled(false);
        options.setExporting(exporting);

        HICredits credits = new HICredits();
        credits.setEnabled(false);
        options.setCredits(credits);

        HIColumn series = new HIColumn();
        series.setData(new ArrayList<>(list));
        options.setSeries(new ArrayList<>(Collections.singletonList(series)));

        chartView.setOptions(options);
        chartView.update(options);
    }
}
