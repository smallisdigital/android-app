package com.smallisdigital.app.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.smallisdigital.app.R;
import com.smallisdigital.app.asynctask.HttpRequest;
import com.smallisdigital.app.session.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class NoteRequestActivity extends AppCompatActivity {

    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_request);

        Bundle extras = getIntent().getExtras();
        String inputId = (extras != null) ? extras.getString("inputId") : null;

        /* Session */
        session = new SessionManager(this);

        /* Toolbar */
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(String.format(Locale.FRENCH, getString(R.string.request_inputid_long), inputId));
        setSupportActionBar(toolbar);

        /* Bottom navigation bar */
        BottomNavigationView bottomNavigationView = findViewById(R.id.nav_view);
        bottomNavigationView.setOnItemSelectedListener(navigationItemSelectedListener);

        HttpRequest request = new HttpRequest(this);

        ImageView[][] smiley = new ImageView[3][5];
        smiley[0][0] = findViewById(R.id.smiley_1_1);
        smiley[0][1] = findViewById(R.id.smiley_2_1);
        smiley[0][2] = findViewById(R.id.smiley_3_1);
        smiley[0][3] = findViewById(R.id.smiley_4_1);
        smiley[0][4] = findViewById(R.id.smiley_5_1);

        smiley[1][0] = findViewById(R.id.smiley_1_2);
        smiley[1][1] = findViewById(R.id.smiley_2_2);
        smiley[1][2] = findViewById(R.id.smiley_3_2);
        smiley[1][3] = findViewById(R.id.smiley_4_2);
        smiley[1][4] = findViewById(R.id.smiley_5_2);

        smiley[2][0] = findViewById(R.id.smiley_1_3);
        smiley[2][1] = findViewById(R.id.smiley_2_3);
        smiley[2][2] = findViewById(R.id.smiley_3_3);
        smiley[2][3] = findViewById(R.id.smiley_4_3);
        smiley[2][4] = findViewById(R.id.smiley_5_3);

        EditText comment = findViewById(R.id.comment);
        Button send = findViewById(R.id.send);

        /* Resource position */
        RelativeLayout.LayoutParams p1 = (RelativeLayout.LayoutParams) smiley[0][0].getLayoutParams();
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        int windowWidth = size.x;
        int priorityWidth = p1.width;

        float dp1 = (windowWidth - 2 * 10 - 5 * priorityWidth) / 5f - 10;

        for(int i = 0 ; i < 3 ; i++) {
            for (int j = 0; j < 5; j++)
                smiley[i][j].setX(dp1 + j * priorityWidth);
        }

        final Integer[][] prevSmiley = new Integer[3][1];

        RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams) smiley[0][0].getLayoutParams();
        int smileySize = p.width;

        for(int i = 0 ; i < 3 ; i++) {
            for (int j = 0; j < 5; j++) {
                int finalI = i;
                int finalJ = j;

                smiley[i][j].setOnClickListener(view -> {
                    if (prevSmiley[finalI][0] != null) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) smiley[finalI][prevSmiley[finalI][0]].getLayoutParams();
                        params.width = smileySize;
                        params.height = smileySize;
                        smiley[finalI][prevSmiley[finalI][0]].setLayoutParams(params);
                    }

                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) smiley[finalI][finalJ].getLayoutParams();
                    params.width = (int) (smileySize * 1.4);
                    params.height = (int) (smileySize * 1.4);
                    smiley[finalI][finalJ].setLayoutParams(params);

                    prevSmiley[finalI][0] = finalJ;
                });
            }
        }

        send.setOnClickListener(view -> {
            if(prevSmiley[0][0] != null && prevSmiley[1][0] != null && prevSmiley[2][0] != null) {
                String hostname = "/api/notation/add";

                JSONObject object = new JSONObject();

                try {
                    object.put("inputId", inputId);
                    object.put("userId", session.getId());
                    object.put("comment", comment.getText().toString());

                    object.put("noteGlobal", prevSmiley[0][0] + 1);
                    object.put("noteDelay", prevSmiley[1][0] + 1);
                    object.put("noteQuality", prevSmiley[2][0] + 1);

                    request.post(hostname, object.toString(), "application/json");

                    Toast.makeText(getApplicationContext(), getString(R.string.notation_ok), Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(getApplicationContext(), HomeUserActivity.class);
                    startActivity(intent);
                    finishAffinity();
                } catch (JSONException ignored) {
                    Toast.makeText(getApplicationContext(), getString(R.string.notation_ko), Toast.LENGTH_LONG).show();
                }
            } else
                Toast.makeText(getApplicationContext(), getString(R.string.notation_missing), Toast.LENGTH_LONG).show();
        });
    }

    private final BottomNavigationView.OnItemSelectedListener navigationItemSelectedListener = item -> {
        Intent intent;

        if (item.getItemId() == R.id.navigation_home) {
            intent = new Intent(getApplicationContext(), HomeUserActivity.class);
            startActivity(intent);
            finish();

            return true;
        } else if(item.getItemId() == R.id.navigation_profile) {
            intent = new Intent(this, VisualizeProfileActivity.class);
            startActivity(intent);

            return true;
        }

        return false;
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        if(item.getItemId() == R.id.logout) {
            session.logout();
            intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finishAffinity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
