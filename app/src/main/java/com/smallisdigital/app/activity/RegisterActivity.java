package com.smallisdigital.app.activity;

import android.content.Intent;

import android.os.Bundle;

import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;

import android.text.method.LinkMovementMethod;
import android.view.inputmethod.InputMethodManager;

import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.smallisdigital.app.R;
import com.smallisdigital.app.asynctask.HttpRequest;
import com.smallisdigital.app.common.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

public class RegisterActivity extends AppCompatActivity {

    private static final Logger logger = Logger.getLogger(RegisterActivity.class.getName());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Button send = findViewById(R.id.register_send);

        TextInputLayout tilFirstname = findViewById(R.id.register_til_firstname);
        TextInputLayout tilLastname = findViewById(R.id.register_til_lastname);
        TextInputLayout tilPassword = findViewById(R.id.register_til_password);
        TextInputLayout tilRepassword = findViewById(R.id.register_til_repassword);
        TextInputLayout tilEmail = findViewById(R.id.register_til_email);
        TextInputLayout tilTerms = findViewById(R.id.register_til_terms);

        CheckBox acceptTerms = findViewById(R.id.accept_terms);

        TextView acceptTermsText = findViewById(R.id.accept_terms_text);

        acceptTermsText.setMovementMethod(LinkMovementMethod.getInstance());

        HttpRequest request = new HttpRequest(this);

        send.setOnClickListener(view -> {
            /* Enlève le clavier */
            InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

            String firstname = (tilFirstname.getEditText() != null) ? tilFirstname.getEditText().getText().toString() : null;
            String lastname = (tilLastname.getEditText() != null) ? tilLastname.getEditText().getText().toString() : null;
            String password = (tilPassword.getEditText() != null) ? tilPassword.getEditText().getText().toString() : null;
            String repassword = (tilRepassword.getEditText() != null) ? tilRepassword.getEditText().getText().toString() : null;
            String email = (tilEmail.getEditText() != null) ? tilEmail.getEditText().getText().toString() : null;

            String hostname = "/api/user/create";

            JSONObject object = new JSONObject();

            try {
                if(acceptTerms.isChecked()) {
                    object.put(Constants.INTENT_FIRSTNAME, firstname);
                    object.put(Constants.INTENT_LASTNAME, lastname);
                    object.put(Constants.INTENT_EMAIL, email);
                    object.put(Constants.INTENT_PASSWORD, password);
                    object.put(Constants.INTENT_REPASSWORD, repassword);

                    JSONObject result = request.post(hostname, object.toString(), "application/json");

                    /* Suppression des précédents messages d'erreur */
                    tilFirstname.setErrorEnabled(false);
                    tilLastname.setErrorEnabled(false);
                    tilPassword.setErrorEnabled(false);
                    tilEmail.setErrorEnabled(false);

                    if (result != null) {
                        if (result.getBoolean("success")) {
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            intent.putExtra("REGISTER", email);
                            startActivity(intent);
                            finishAffinity();
                        } else {
                            JSONArray errors = result.getJSONArray("error");

                            for (int i = 0, l = errors.length(); i < l; i++) {
                                JSONObject error = errors.getJSONObject(i);

                                switch (error.getString("name")) {
                                    case Constants.INTENT_LASTNAME -> tilLastname.setError(error.getString("value"));
                                    case Constants.INTENT_EMAIL -> tilEmail.setError(error.getString("value"));
                                    case Constants.INTENT_PASSWORD -> tilPassword.setError(error.getString("value"));
                                    default -> tilFirstname.setError(error.getString("value"));
                                }
                            }
                        }
                    }
                } else {
                    tilTerms.setError(getString(R.string.register_error_accept_terms));
                }
            } catch (JSONException e) {
                logger.log(Level.SEVERE, e.getMessage());
            }
        });
    }
}
