package com.smallisdigital.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.smallisdigital.app.R;
import com.smallisdigital.app.adapter.CalendarAdapter;
import com.smallisdigital.app.adapter.PlanningAdapter;
import com.smallisdigital.app.asynctask.JsonArrayAsyncTask;
import com.smallisdigital.app.session.SessionManager;
import com.smallisdigital.app.view.CalendarView;

import java.util.List;

public class PlanningActivity extends AppCompatActivity implements CalendarAdapter.OnAdapterInteractionListener {

    private RecyclerView wrapper;

    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_planning);

        /* Session */
        session = new SessionManager(this);

        /* Toolbar */
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /* Bottom navigation bar */
        BottomNavigationView bottomNavigationView = findViewById(R.id.nav_view);
        bottomNavigationView.setOnItemSelectedListener(navigationItemSelectedListener);

        CalendarView customCalendarView = findViewById(R.id.calendar_view);

        wrapper = findViewById(R.id.view);
        wrapper.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onAdapterCreateInteraction(List<String> inputIds) {
        PlanningAdapter adapter = new PlanningAdapter(this);
        wrapper.setAdapter(adapter);

        StringBuilder sInputId = new StringBuilder();

        for(String inputId : inputIds)
            sInputId.append(inputId).append(",");

        if(sInputId.length() != 0) {
            String hostname = "/api/request/list";
            String params = "inputIds=" + sInputId.substring(0, sInputId.length() - 1);

            JsonArrayAsyncTask asyncTask = new JsonArrayAsyncTask(this, adapter);
            asyncTask.execute(hostname, params);

            adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                @Override
                public void onChanged() {
                    /* Do nothing */
                }
            });
        }
    }

    private final BottomNavigationView.OnItemSelectedListener navigationItemSelectedListener = item -> {
        Intent intent;

        if (item.getItemId() == R.id.navigation_home) {
            intent = new Intent(getApplicationContext(), HomeStaffActivity.class);
            startActivity(intent);
            finish();

            return true;
        } else if(item.getItemId() == R.id.navigation_profile) {
            intent = new Intent(this, VisualizeProfileActivity.class);
            startActivity(intent);

            return true;
        }

        return false;
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        if(item.getItemId() == R.id.logout) {
            session.logout();
            intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finishAffinity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
