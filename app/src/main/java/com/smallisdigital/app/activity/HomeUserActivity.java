package com.smallisdigital.app.activity;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.smallisdigital.app.R;
import com.smallisdigital.app.adapter.HomeUserAdapter;
import com.smallisdigital.app.asynctask.JsonArrayAsyncTask;
import com.smallisdigital.app.session.SessionManager;

public class HomeUserActivity extends AppCompatActivity {

    private  JsonArrayAsyncTask asyncTask = null;

    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home_user);

        /* Session */
        session = new SessionManager(this);

        /* Toolbar */
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /* Bottom navigation bar */
        BottomNavigationView bottomNavigationView = findViewById(R.id.nav_view);
        bottomNavigationView.setOnItemSelectedListener(navigationItemSelectedListener);

        Button button1 = findViewById(R.id.button1);
        Button button2 = findViewById(R.id.button2);
        Button button3 = findViewById(R.id.button3);

        /* Button size */
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        button1.setOnClickListener(view -> {
            Intent intent = new Intent(this, CreateRequestActivity.class);

            startActivity(intent);
        });

        button2.setOnClickListener(view -> {
            Intent intent = new Intent(this, VisualizeRequestActivity.class);

            startActivity(intent);
        });

        button3.setOnClickListener(view -> {
            Intent intent = new Intent(this, NoteSurveyActivity.class);

            startActivity(intent);
        });

        final RecyclerView wrapper = findViewById(R.id.view);
        wrapper.setLayoutManager(new LinearLayoutManager(this));
        HomeUserAdapter adapter = new HomeUserAdapter(this);
        wrapper.setAdapter(adapter);

        /* Visibility gone when loading */
        wrapper.setVisibility(View.GONE);

        String hostname = "/api/request/list";
        String params = "userId=" + session.getId();

        asyncTask = new JsonArrayAsyncTask(this, adapter);
        asyncTask.execute(hostname, params);

        /* Chargement de la page */
        final ProgressBar progress = findViewById(R.id.progress);
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                progress.setVisibility(View.GONE);
                wrapper.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(asyncTask !=  null)
            asyncTask.cancel(true);
    }

    private final BottomNavigationView.OnItemSelectedListener navigationItemSelectedListener = item -> {
        Intent intent;

        if(item.getItemId() == R.id.navigation_profile) {
            intent = new Intent(this, VisualizeProfileActivity.class);
            startActivity(intent);

            return true;
        }

        return false;
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        if(item.getItemId() == R.id.logout) {
            session.logout();
            intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finishAffinity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
