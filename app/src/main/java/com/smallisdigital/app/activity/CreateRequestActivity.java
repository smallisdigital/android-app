package com.smallisdigital.app.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.textfield.TextInputLayout;
import com.smallisdigital.app.asynctask.HttpRequest;
import com.smallisdigital.app.fragment.CreateRequestFragment;
import com.smallisdigital.app.R;
import com.smallisdigital.app.fragment.FinishRequestFragment;
import com.smallisdigital.app.session.SessionManager;
import com.smallisdigital.app.util.Keyboard;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CreateRequestActivity extends AppCompatActivity implements CreateRequestFragment.OnFragmentInteractionListener, FinishRequestFragment.OnFragmentInteractionListener {

    private static final Logger logger = Logger.getLogger(CreateRequestActivity.class.getName());

    private Integer category;
    private Integer subCategory;
    private String encodedImage;

    private TextInputLayout tilAddress;
    private TextInputLayout tilDescription;
    private ImageView imageView;

    private SessionManager session;
    private HttpRequest request;

    public CreateRequestActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_create_request);

        /* Toolbar */
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /* Bottom navigation bar */
        BottomNavigationView bottomNavigationView = findViewById(R.id.nav_view);
        bottomNavigationView.setOnItemSelectedListener(navigationItemSelectedListener);

        getSupportFragmentManager().beginTransaction().setReorderingAllowed(true).add(R.id.container, CreateRequestFragment.class, null).commit();

        session = new SessionManager(this);
        request = new HttpRequest(this);
    }

    @Override
    public void onFragmentCreateInteraction(View view, int step) {
        if(view instanceof Button button) {
            if (step == 0) {
                category = Integer.parseInt((String) button.getContentDescription());
            }
            else if (step == 1) {
                subCategory = Integer.parseInt((String) button.getContentDescription());

                getSupportFragmentManager().beginTransaction().replace(R.id.container, FinishRequestFragment.class, null).commit();
            }
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onFragmentFinishInteraction(View view) {
        if (view instanceof Button) {

            switch (view.getId()) {
                case R.id.picture -> loadPicture();
                case R.id.send -> {
                    String hostname = "/api/request/create";

                    JSONObject object = new JSONObject();

                    boolean error = false;

                    tilAddress.setErrorEnabled(false);
                    tilDescription.setErrorEnabled(false);

                    if(tilAddress.getEditText() == null || tilAddress.getEditText().getText().toString().trim().isEmpty()) {
                        tilAddress.setError(getText(R.string.request_empty_address));
                        error = true;
                    }

                    if(tilDescription.getEditText() == null || tilDescription.getEditText().getText().toString().trim().isEmpty()) {
                        tilDescription.setError(getText(R.string.request_empty_description));
                        error = true;
                    }

                    Keyboard.hideKeyboard(this);

                    if(!error) {
                        try {
                            object.put("user", String.valueOf(session.getId()));
                            object.put("category", String.valueOf(category));
                            object.put("subcategory", String.valueOf(subCategory));
                            object.put("address", URLEncoder.encode(tilAddress.getEditText().getText().toString().trim(), StandardCharsets.UTF_8.toString()));
                            object.put("description", URLEncoder.encode(tilDescription.getEditText().getText().toString().trim(), StandardCharsets.UTF_8.toString()));
                            object.put("image", encodedImage);

                            request.post(hostname, object.toString(), "application/json");

                            Toast.makeText(getApplicationContext(), "Votre demande a bien été prise en compte", Toast.LENGTH_LONG).show();
                        } catch (JSONException | UnsupportedEncodingException ignored) {}

                        Intent intent = new Intent(getApplicationContext(), HomeUserActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
                default -> throw new UnsupportedOperationException();
            }
        }
    }

    ActivityResultLauncher<Intent> openActivityPicture = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
                    try {

                        Bitmap bitmap;

                        if(result.getData().getAction() != null) {
                            bitmap = (Bitmap) result.getData().getExtras().get("data");
                        } else {
                            final Uri imageUri = result.getData().getData();
                            final InputStream imageStream = getContentResolver().openInputStream(imageUri);

                            bitmap = BitmapFactory.decodeStream(imageStream);
                        }

                        imageView.setImageBitmap(bitmap);

                        if(bitmap != null) {
                            double rw = 1080. / bitmap.getWidth();
                            double rh = 1920. / bitmap.getHeight();

                            int width;
                            int height;

                            if(rw < rh && rw < 1) {
                                width = (int) (bitmap.getWidth() * rw);
                                height = (int) (bitmap.getHeight() * rw);
                            } else if(rh < rw && rh < 1) {
                                width = (int) (bitmap.getWidth() * rh);
                                height = (int) (bitmap.getHeight() * rh);
                            } else {
                                width = bitmap.getWidth();
                                height = bitmap.getHeight();
                            }

                            bitmap = Bitmap.createScaledBitmap(bitmap, width, height, false);

                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                            byte[] bytes = baos.toByteArray();

                            encodedImage = Base64.encodeToString(bytes, Base64.NO_WRAP);
                        }
                    } catch (IOException e) {
                        logger.log(Level.SEVERE, e.getMessage());
                        Toast.makeText(this, "Le fichier n'a pas pu être trouvé", Toast.LENGTH_LONG).show();
                    }
                }
            }
    );

    private void loadPicture() {
        final CharSequence[] options = {"Prendre une photo", "Sélectionner une image dans la galerie"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Ajouter une image");
        builder.setItems(options, (dialog, item) -> {
            Intent intent = null;

            if (options[item].equals("Prendre une photo")) {
                intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            } else if (options[item].equals("Sélectionner une image dans la galerie")) {
                intent = new   Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
            }

            if(intent != null)
                openActivityPicture.launch(intent);
        });

        builder.show();
    }

    private final BottomNavigationView.OnItemSelectedListener navigationItemSelectedListener = item -> {
        Intent intent;

        if (item.getItemId() == R.id.navigation_home) {
            intent = new Intent(getApplicationContext(), HomeUserActivity.class);
            startActivity(intent);
            finish();

            return true;
        } else if(item.getItemId() == R.id.navigation_profile) {
            intent = new Intent(this, VisualizeProfileActivity.class);
            startActivity(intent);

            return true;
        }

        return false;
    };

    public void setAddress(TextInputLayout tilAddress) {
        this.tilAddress = tilAddress;
    }

    public void setDescription(TextInputLayout tilDescription) {
        this.tilDescription = tilDescription;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        if(item.getItemId() == R.id.logout) {
            session.logout();
            intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finishAffinity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
