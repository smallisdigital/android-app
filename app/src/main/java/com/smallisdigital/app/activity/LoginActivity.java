package com.smallisdigital.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;

import com.smallisdigital.app.R;
import com.smallisdigital.app.asynctask.HttpRequest;
import com.smallisdigital.app.common.Constants;
import com.smallisdigital.app.session.SessionManager;
import com.smallisdigital.app.util.Keyboard;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoginActivity extends AppCompatActivity {
    private static final Logger logger = Logger.getLogger(LoginActivity.class.getName());

    private TextInputLayout tilEmail;
    private TextInputLayout tilPassword;
    private RelativeLayout relativeLogin;
    private RelativeLayout relativeInfo;
    private EditText loginEmail;
    private EditText loginPassword;
    private SessionManager session;
    private HttpRequest request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Intent intent = getIntent();
        if (intent.hasExtra("REGISTER")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(String.format(Locale.FRENCH, getString(R.string.register_confirm_email), intent.getStringExtra("REGISTER")));
            builder.create().show();
        }

        Button send = findViewById(R.id.login_send);
        Button register = findViewById(R.id.register);

        relativeLogin = findViewById(R.id.relative_login);
        relativeInfo = findViewById(R.id.relative_info);

        tilEmail = findViewById(R.id.login_til_email);
        tilPassword = findViewById(R.id.login_til_password);

        loginEmail = findViewById(R.id.login_email);
        loginPassword = findViewById(R.id.login_password);

        request = new HttpRequest(this);

        /* Connexion automatique - Session existante */
        session = new SessionManager(this);
        if (session.isLogged())
            login(session.getEmail(), session.getPassword());

        send.setOnClickListener(view -> {
            String email = (tilEmail.getEditText() != null) ? tilEmail.getEditText().getText().toString() : null;
            String password = (tilPassword.getEditText() != null) ? tilPassword.getEditText().getText().toString() : null;

            login(email, password);
        });

        register.setOnClickListener(view -> {
            Intent intent1 = new Intent(getApplicationContext(), RegisterActivity.class);
            startActivity(intent1);
        });
    }

    public void login (String email, String password){
        /* Suppression du clavier */
        Keyboard.hideKeyboard(this);

        /* Suppression du curseur */
        loginEmail.setCursorVisible(false);
        loginPassword.setCursorVisible(false);

        /* Suppression des précédents messages d'erreur */
        tilEmail.setErrorEnabled(false);
        tilPassword.setErrorEnabled(false);

        relativeLogin.setVisibility(View.GONE);
        relativeInfo.setVisibility(View.VISIBLE);

        String encodedPassword = password;

        try {
            encodedPassword = URLEncoder.encode(password, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException ignored) {}

        String hostname = "/api/user/login";
        String params = "email=" + email.trim() + "&password=" + encodedPassword;

        JSONObject result = request.get(hostname, params);

        if(result != null) {
            try {
                if((result.getBoolean("success"))) {
                    int id = result.getInt("id");

                    String firstname = result.getString(Constants.INTENT_FIRSTNAME);
                    String lastname = result.getString(Constants.INTENT_LASTNAME);
                    int level = (result.isNull(Constants.INTENT_LEVEL)) ? 0 : result.getInt(Constants.INTENT_LEVEL);
                    int categoryId = (result.isNull(Constants.INTENT_CATEGORY_ID)) ? 0 : result.getInt(Constants.INTENT_CATEGORY_ID);
                    String categoryName = (result.isNull(Constants.INTENT_CATEGORY_NAME)) ? null : result.getString(Constants.INTENT_CATEGORY_NAME);
                    int subCategoryId = (result.isNull(Constants.INTENT_SUB_CATEGORY_ID)) ? 0 : result.getInt(Constants.INTENT_SUB_CATEGORY_ID);
                    String subCategoryName = (result.isNull(Constants.INTENT_SUB_CATEGORY_NAME)) ? null : result.getString(Constants.INTENT_SUB_CATEGORY_NAME);

                    session.insertUser(id, firstname, lastname, email, password, level, categoryId, categoryName, subCategoryId, subCategoryName);

                    Intent intent;

                    if(session.isTaff())
                        intent = new Intent(this, HomeStaffActivity.class);
                    else
                        intent = new Intent(this, HomeUserActivity.class);

                    startActivity(intent);
                    finishAffinity();
                } else {
                    loginEmail.setCursorVisible(true);
                    loginPassword.setCursorVisible(true);

                    relativeLogin.setVisibility(View.VISIBLE);
                    relativeInfo.setVisibility(View.GONE);

                    tilPassword.setError(result.getString("error"));
                }

            } catch(JSONException e) {
                logger.log(Level.SEVERE, e.getMessage());
            }
        }
    }
}
