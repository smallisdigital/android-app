package com.smallisdigital.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.textfield.TextInputLayout;
import com.smallisdigital.app.R;
import com.smallisdigital.app.asynctask.HttpRequest;
import com.smallisdigital.app.session.SessionManager;
import com.smallisdigital.app.util.Keyboard;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CreateSurveyActivity extends AppCompatActivity {

    private SessionManager session;

    private int nSurvey = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_survey);

        /* Toolbar */
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /* Bottom navigation bar */
        BottomNavigationView bottomNavigationView = findViewById(R.id.nav_view);
        bottomNavigationView.setOnItemSelectedListener(navigationItemSelectedListener);

        session = new SessionManager(this);

        HttpRequest request = new HttpRequest(this);

        ImageView add = findViewById(R.id.add);
        Button send = findViewById(R.id.send);

        TextInputLayout tilName = findViewById(R.id.til_name);
        TextInputLayout tilQuestion = findViewById(R.id.til_question);
        TextInputLayout tilDescription = findViewById(R.id.til_description);
        TextInputLayout tilResponse = findViewById(R.id.til_response);

        List<TextInputLayout> tilResponseMore = new ArrayList<>();
        List<Integer> tilResponseId = new ArrayList<>();

        tilResponse.setHint(String.format(Locale.FRENCH, "%s1", getText(R.string.survey_response)));

        add.setOnClickListener(view -> {
            int dp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics());

            RelativeLayout relativeLayout = findViewById(R.id.view_response);

            EditText editText = new EditText(CreateSurveyActivity.this);
            LinearLayout.LayoutParams editTextParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            editText.setInputType(InputType.TYPE_CLASS_TEXT);
            editText.setMaxLines(1);
            editText.setImeOptions(EditorInfo.IME_ACTION_DONE);

            RelativeLayout.LayoutParams textInputLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            textInputLayoutParams.setMargins(dp, 0, dp, dp);

            if(tilResponseMore.isEmpty())
                textInputLayoutParams.addRule(RelativeLayout.BELOW, R.id.til_response);
            else
                textInputLayoutParams.addRule(RelativeLayout.BELOW, tilResponseId.get(nSurvey - 2));

            TextInputLayout textInputLayout = new TextInputLayout(CreateSurveyActivity.this);
            textInputLayout.setId(View.generateViewId());
            textInputLayout.setHint(String.format(Locale.FRENCH, "%s%d", getText(R.string.survey_response), ++nSurvey));

            textInputLayout.addView(editText, editTextParams);
            relativeLayout.addView(textInputLayout, textInputLayoutParams);

            tilResponseMore.add(textInputLayout);
            tilResponseId.add(textInputLayout.getId());
        });

        send.setOnClickListener(view -> {
            String hostname = "/api/survey/create";

            JSONObject object = new JSONObject();

            boolean error = false;

            tilName.setErrorEnabled(false);
            tilQuestion.setErrorEnabled(false);
            tilDescription.setErrorEnabled(false);
            tilResponse.setErrorEnabled(false);

            if(tilName.getEditText() == null || tilName.getEditText().getText().toString().trim().isEmpty()) {
                tilName.setError(getText(R.string.survey_empty_name));
                error = true;
            }

            if(tilQuestion.getEditText() == null || tilQuestion.getEditText().getText().toString().trim().isEmpty()) {
                tilQuestion.setError(getText(R.string.survey_empty_question));
                error = true;
            }

            if(tilResponse.getEditText() == null || tilResponse.getEditText().getText().toString().trim().isEmpty()) {
                tilResponse.setError(getText(R.string.survey_empty_response));
                error = true;
            }

            Keyboard.hideKeyboard(CreateSurveyActivity.this);

            if(!error) {
                try {
                    object.put("user", String.valueOf(session.getId()));
                    object.put("name", tilName.getEditText().getText().toString().trim());
                    object.put("question", tilQuestion.getEditText().getText().toString().trim());

                    if(tilDescription.getEditText() != null && !tilDescription.getEditText().getText().toString().trim().isEmpty())
                        object.put("description", tilDescription.getEditText().getText().toString().trim());

                    JSONArray jsonArray = new JSONArray();
                    jsonArray.put(tilResponse.getEditText().getText().toString().trim());

                    if(!tilResponseMore.isEmpty()) {
                        for(TextInputLayout til : tilResponseMore) {
                            if(til.getEditText() != null)
                                jsonArray.put(til.getEditText().getText().toString().trim());
                        }
                    }

                    object.put("response", jsonArray);

                    request.post(hostname, object.toString(), "application/json");

                    Toast.makeText(getApplicationContext(), "Votre sondage a bien été pris en compte", Toast.LENGTH_LONG).show();
                } catch (JSONException ignored) {}

                Intent intent = new Intent(getApplicationContext(), HomeStaffActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private final BottomNavigationView.OnItemSelectedListener navigationItemSelectedListener = item -> {
        Intent intent;

        if (item.getItemId() == R.id.navigation_home) {
            intent = new Intent(getApplicationContext(), HomeStaffActivity.class);
            startActivity(intent);
            finish();

            return true;
        } else if(item.getItemId() == R.id.navigation_profile) {
            intent = new Intent(this, VisualizeProfileActivity.class);
            startActivity(intent);

            return true;
        }

        return false;
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        if(item.getItemId() == R.id.logout) {
            session.logout();
            intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finishAffinity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
