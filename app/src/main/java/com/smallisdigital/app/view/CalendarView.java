package com.smallisdigital.app.view;

import java.text.SimpleDateFormat;

import android.content.Context;

import android.util.AttributeSet;

import android.view.LayoutInflater;
import android.view.View;

import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.smallisdigital.app.R;
import com.smallisdigital.app.adapter.CalendarAdapter;
import com.smallisdigital.app.asynctask.HttpRequest;
import com.smallisdigital.app.dto.Event;
import com.smallisdigital.app.session.SessionManager;
import com.smallisdigital.app.util.OnSwipeTouchListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CalendarView extends LinearLayout {

    private static final int MAX_CALENDAR_DAYS = 42;

    private Context context;

    private SessionManager session;

    private GridView gridView;

    private final Calendar calendar = Calendar.getInstance(Locale.FRANCE);

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM yyyy", Locale.FRANCE);

    List<Date> dates = new ArrayList<>();
    List<Event> eventsList = new ArrayList<>();

    public CalendarView(Context context) {
        super(context);
    }

    public CalendarView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CalendarView(final Context context, @Nullable final AttributeSet attrs) {
        super(context, attrs);

        this.context = context;

        session = new SessionManager(context);

        InitializeLayout();
        SetUpCalendar();

        gridView.setOnTouchListener(new OnSwipeTouchListener(context) {
            @Override
            public void onSwipeRight() {
                calendar.add(Calendar.MONTH, -1);
                SetUpCalendar();
            }

            @Override
            public void onSwipeLeft() {
                calendar.add(Calendar.MONTH, 1);
                SetUpCalendar();
            }
        });
    }

    private void InitializeLayout() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.calendar_layout, this);

        gridView = view.findViewById(R.id.gridView);
    }

    private void SetUpCalendar() {
        ActionBar actionBar = ((AppCompatActivity) context).getSupportActionBar();

        String date = Character.toUpperCase(dateFormat.format(calendar.getTime()).charAt(0)) + dateFormat.format(calendar.getTime()).substring(1);

        if(actionBar != null)
            actionBar.setTitle(date);

        dates.clear();

        Calendar monthCalendar = (Calendar) calendar.clone();
        monthCalendar.set(Calendar.DAY_OF_MONTH, 1);

        int firstDayOfMonth = monthCalendar.get(Calendar.DAY_OF_WEEK) - 1;
        monthCalendar.add(Calendar.DAY_OF_MONTH, -firstDayOfMonth);

        CollectEventPerMonth();

        while(dates.size() < MAX_CALENDAR_DAYS) {
            dates.add(monthCalendar.getTime());
            monthCalendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        CalendarAdapter calendarAdapter = new CalendarAdapter(getContext(), dates, calendar, eventsList);
        gridView.setAdapter(calendarAdapter);
    }

    private void CollectEventPerMonth() {
        String hostname = "/api/request/list";
        String params = "subCategoryId=" + session.getSubCategoryId() + "&status=" + 2;

        HttpRequest request = new HttpRequest(context);
        JSONArray json = request.get_array(hostname, params);

        try {
            for(int i = 0, l = json.length() ; i < l ; i++) {
                JSONObject jsonObject = json.getJSONObject(i);

                eventsList.add(new Event(jsonObject.getString("inputId"), "00:00", jsonObject.getString("creationDate"), jsonObject.getInt("delay")));
            }
        } catch (JSONException ignore) {}

        /* Chargement de la page */
        final ProgressBar progress = findViewById(R.id.progress);
        progress.setVisibility(View.GONE);
    }
}
