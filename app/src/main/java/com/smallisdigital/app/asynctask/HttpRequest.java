package com.smallisdigital.app.asynctask;

import android.content.Context;
import android.os.StrictMode;

import com.smallisdigital.app.util.PropertiesManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HttpRequest {

    private static final Logger logger = Logger.getLogger(HttpRequest.class.getName());

    private final Context context;

    public HttpRequest(Context context) {
        this.context = context;
    }

    public JSONObject get(String hostname, String params) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {
            URL url = new URL(PropertiesManager.getProperty("api.url", context) + hostname + "?" + params);
            HttpURLConnection cnx = (HttpURLConnection) url.openConnection();

            cnx.setRequestMethod("GET");

            String line;

            BufferedReader data;

            if(cnx.getResponseCode() >= 200 && cnx.getResponseCode() < 300)
                data = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
            else
                data = new BufferedReader(new InputStreamReader(cnx.getErrorStream()));

            StringBuilder output = new StringBuilder();

            while((line = data.readLine()) != null)
                output.append(line);

            return new JSONObject(output.toString());

        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }

        return null;
    }

    public JSONArray get_array(String hostname, String params) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {
            URL url = new URL(PropertiesManager.getProperty("api.url", context) + hostname + "?" + params);
            HttpURLConnection cnx = (HttpURLConnection) url.openConnection();

            cnx.setRequestMethod("GET");

            String line;

            BufferedReader data;

            if(cnx.getResponseCode() >= 200 && cnx.getResponseCode() < 300)
                data = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
            else
                data = new BufferedReader(new InputStreamReader(cnx.getErrorStream()));

            StringBuilder output = new StringBuilder();

            while((line = data.readLine()) != null)
                output.append(line);

            return new JSONArray(output.toString());

        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }

        return null;
    }

    public JSONObject post(String hostname, String params, String type) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {
            URL url = new URL(PropertiesManager.getProperty("api.url", context) + hostname);
            HttpURLConnection cnx = (HttpURLConnection) url.openConnection();

            cnx.setRequestMethod("POST");
            cnx.setRequestProperty("Content-Type", type);
            cnx.setDoOutput(true);

            DataOutputStream stream = new DataOutputStream(cnx.getOutputStream());
            stream.writeBytes(params);
            stream.flush();
            stream.close();

            String line;

            BufferedReader data;

            if(cnx.getResponseCode() >= 200 && cnx.getResponseCode() < 300)
                data = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
            else
                data = new BufferedReader(new InputStreamReader(cnx.getErrorStream()));

            StringBuilder output = new StringBuilder();

            while((line = data.readLine()) != null)
                output.append(line);

            return new JSONObject(output.toString());

        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }

        return null;
    }
}

