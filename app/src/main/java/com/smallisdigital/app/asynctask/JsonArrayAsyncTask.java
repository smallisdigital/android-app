package com.smallisdigital.app.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.smallisdigital.app.util.PropertiesManager;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

public class JsonArrayAsyncTask extends AsyncTask<String, Void, JSONArray> {
    public interface JSONConsumer {
        void setJSONDocument(JSONArray document);
    }

    private final WeakReference<Context> context;
    private final JSONConsumer consumer;

    public JsonArrayAsyncTask(Context context, JSONConsumer consumer) {
        this.context = new WeakReference<>(context);
        this.consumer = consumer;
    }

    @Override
    protected JSONArray doInBackground(String... params) {
        try {
            URL url = new URL(PropertiesManager.getProperty("api.url", context.get()) + params[0] + "?" + params[1]);

            HttpURLConnection cnx = (HttpURLConnection) url.openConnection();

            cnx.setRequestMethod("GET");

            String line;

            BufferedReader data;

            if(cnx.getResponseCode() == 200)
                data = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
            else
                data = new BufferedReader(new InputStreamReader(cnx.getErrorStream()));

            StringBuilder output = new StringBuilder();

            while((line = data.readLine()) != null)
                output.append(line);

            return new JSONArray(output.toString());
        } catch (Exception e) {
            Toast.makeText(context.get(), "Aucune connexion réseau", Toast.LENGTH_LONG).show();
        }

        return null;
    }

    @Override
    protected void onPostExecute(JSONArray result) {
        if(result != null)
            consumer.setJSONDocument(result);
    }
}
