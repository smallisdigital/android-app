package com.smallisdigital.app.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.util.Base64;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smallisdigital.app.R;
import com.smallisdigital.app.activity.HomeStaffActivity;
import com.smallisdigital.app.activity.TreatRequestActivity;
import com.smallisdigital.app.asynctask.HttpRequest;
import com.smallisdigital.app.asynctask.JsonObjectAsyncTask;
import com.smallisdigital.app.common.Constants;
import com.smallisdigital.app.session.SessionManager;
import com.smallisdigital.app.util.Dialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TreatRequestAdapter extends RecyclerView.Adapter<TreatRequestAdapter.ViewHolder> implements JsonObjectAsyncTask.JSONConsumer {

    private static final Logger logger = Logger.getLogger(TreatRequestAdapter.class.getName());

    private final Context context;

    private final SessionManager session;

    private JSONObject document = null;

    public TreatRequestAdapter(Context context, SessionManager session) {
        this.context = context;
        this.session = session;
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_treat_request, parent, false);
        return new ViewHolder(context, session, view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        try {
            if(document != null)
                holder.setElement(document);

        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void setJSONDocument(JSONObject object) {
        document = object;

        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final Context context;

        private final SessionManager session;

        private final HttpRequest request;

        private final TextView date;
        private final TextView category;
        private final TextView type;
        private final TextView status;
        private final TextView address;
        private final TextView description;

        private final EditText scheduleDate;

        private final ImageView picture;
        private final Button planning;

        private final ImageView[] priority = new ImageView[5];
        private final ImageView[] delay = new ImageView[10];

        private ViewHolder(final Context context, final SessionManager session, final View itemView) {
            super(itemView);

            this.context = context;

            this.session = session;

            request = new HttpRequest(context);

            date = itemView.findViewById(R.id.date);
            category = itemView.findViewById(R.id.category);
            type = itemView.findViewById(R.id.type);
            status = itemView.findViewById(R.id.status);
            address = itemView.findViewById(R.id.address);
            description = itemView.findViewById(R.id.description);
            picture = itemView.findViewById(R.id.picture);
            planning = itemView.findViewById(R.id.planning);

            scheduleDate = itemView.findViewById(R.id.schedule_date);

            priority[0] = itemView.findViewById(R.id.priority_1);
            priority[1] = itemView.findViewById(R.id.priority_2);
            priority[2] = itemView.findViewById(R.id.priority_3);
            priority[3] = itemView.findViewById(R.id.priority_4);
            priority[4] = itemView.findViewById(R.id.priority_5);

            delay[0] = itemView.findViewById(R.id.delay_1);
            delay[1] = itemView.findViewById(R.id.delay_2);
            delay[2] = itemView.findViewById(R.id.delay_3);
            delay[3] = itemView.findViewById(R.id.delay_4);
            delay[4] = itemView.findViewById(R.id.delay_5);
            delay[5] = itemView.findViewById(R.id.delay_6);
            delay[6] = itemView.findViewById(R.id.delay_7);
            delay[7] = itemView.findViewById(R.id.delay_8);
            delay[8] = itemView.findViewById(R.id.delay_9);
            delay[9] = itemView.findViewById(R.id.delay_a);

            /* Resource position */
            RelativeLayout.LayoutParams p1 = (RelativeLayout.LayoutParams) priority[0].getLayoutParams();
            RelativeLayout.LayoutParams p2 = (RelativeLayout.LayoutParams) delay[0].getLayoutParams();
            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);

            int windowWidth = size.x;
            int priorityWidth = p1.width;
            int delayWidth = p2.width;

            float dp1 = (windowWidth - 2 * 10 - 5 * priorityWidth) / 5f - 10;
            float dp2 = (windowWidth - 2 * 10 - 5 * delayWidth) / 5f - 10;

            for(int i = 0 ; i < 5 ; i++)
                priority[i].setX(dp1 + i * priorityWidth);

            for(int i = 0 ; i < 10 ; i++) {
                if(i < 5)
                    delay[i].setX(dp2 + i * delayWidth);
                else
                    delay[i].setX(dp2 + (i - 5) * delayWidth);
            }
        }

        private void setElement(JSONObject element) {
            try {
                final Integer[] prevPriority = { null };
                final Integer[] prevDelay = { null };

                final Integer[] sDelay = new Integer[] { 1800, 3600, 7200, 14400, 21600, 86400, 172800, 259200, 345600, 432000 };

                RelativeLayout.LayoutParams p1 = (RelativeLayout.LayoutParams) priority[0].getLayoutParams();
                RelativeLayout.LayoutParams p2 = (RelativeLayout.LayoutParams) delay[0].getLayoutParams();
                int prioritySize = p1.width;
                int delaySize = p2.width;
                int inputId = element.getInt("inputId");

                String sStatus;
                String sAddress = URLDecoder.decode(element.getString("address"), StandardCharsets.UTF_8.toString());
                String sDescription = URLDecoder.decode(element.getString("description"), StandardCharsets.UTF_8.toString());

                if(element.getInt("status") == Constants.REQUEST_TODO)
                    sStatus = context.getString(R.string.request_status_todo);
                else if(element.getInt("status") == Constants.REQUEST_RUNNING)
                    sStatus = context.getString(R.string.request_status_running);
                else
                    sStatus = context.getString(R.string.request_status_complete);

                date.setText(String.format(Locale.FRENCH, context.getString(R.string.request_date), element.getString("creationDate")));
                category.setText(String.format(Locale.FRENCH, context.getString(R.string.request_category), element.getString("category")));
                type.setText(String.format(Locale.FRENCH, context.getString(R.string.request_type), element.getString("subcategory")));
                status.setText(String.format(Locale.FRENCH, context.getString(R.string.request_status), sStatus));

                if(!element.isNull("image")) {
                    byte[] image = Base64.decode(element.getString("image"), Base64.NO_WRAP);
                    Bitmap bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);

                    picture.setImageBitmap(bitmap);
                }

                if(!element.isNull("address"))
                    address.setText(String.format(Locale.FRENCH, context.getString(R.string.request_address), sAddress));
                else
                    address.setText(String.format(Locale.FRENCH, context.getString(R.string.request_address), context.getString(R.string.request_address_ko)));

                if(!element.isNull("description"))
                    description.setText(String.format(Locale.FRENCH, context.getString(R.string.request_description), sDescription));
                else
                    description.setText(String.format(Locale.FRENCH, context.getString(R.string.request_description), context.getString(R.string.request_description_ko)));

                if(element.getInt("status") == Constants.REQUEST_TODO)
                    scheduleDate.setOnClickListener(view -> Dialog.selectDate(context, scheduleDate));
                else
                    scheduleDate.setText(element.getString("scheduleDate"));

                /* Priority */
                if(element.getInt(Constants.INTENT_STATUS) == Constants.REQUEST_TODO) {
                    for (int i = 0, l = priority.length ; i < l ; i++) {
                        int finalI = i;

                        priority[i].setOnClickListener(view -> {
                            if (prevPriority[0] != null) {
                                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) priority[prevPriority[0]].getLayoutParams();
                                params.width = prioritySize;
                                params.height = prioritySize;
                                priority[prevPriority[0]].setLayoutParams(params);
                            }

                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) priority[finalI].getLayoutParams();
                            params.width = (int) (prioritySize * 1.4);
                            params.height = (int) (prioritySize * 1.4);
                            priority[finalI].setLayoutParams(params);

                            prevPriority[0] = finalI;
                        });
                    }
                }

                /* Priority already defined */
                if(!element.isNull(Constants.INTENT_PRIORITY)) {
                    prevPriority[0] = element.getInt(Constants.INTENT_PRIORITY) - 1;

                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) priority[prevPriority[0]].getLayoutParams();
                    params.width = (int) (prioritySize * 1.4);
                    params.height = (int) (prioritySize * 1.4);
                    priority[prevPriority[0]].setLayoutParams(params);
                }

                /* Delay */
                if(element.getInt(Constants.INTENT_STATUS) == Constants.REQUEST_TODO) {
                    for (int i = 0, l = delay.length ; i < l ; i++) {
                        int finalI = i;

                        delay[i].setOnClickListener(view -> {
                            if (prevDelay[0] != null) {
                                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) delay[prevDelay[0]].getLayoutParams();
                                params.width = delaySize;
                                params.height = delaySize;
                                delay[prevDelay[0]].setLayoutParams(params);
                            }

                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) delay[finalI].getLayoutParams();
                            params.width = (int) (delaySize * 1.4);
                            params.height = (int) (delaySize * 1.4);
                            delay[finalI].setLayoutParams(params);

                            prevDelay[0] = finalI;
                        });
                    }
                }

                /* Delay already defined */
                if(!element.isNull(Constants.INTENT_DELAY)) {
                    for(int i = 0, l = sDelay.length ; i < l ; i++) {
                        if (sDelay[i] == element.getInt(Constants.INTENT_DELAY)) {
                            prevDelay[0] = i;
                            break;
                        }
                    }

                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) delay[prevDelay[0]].getLayoutParams();
                    params.width = (int) (delaySize * 1.4);
                    params.height = (int) (delaySize * 1.4);
                    delay[prevDelay[0]].setLayoutParams(params);
                }

                if(element.getInt(Constants.INTENT_STATUS) == Constants.REQUEST_TODO) {
                    planning.setOnClickListener(view -> {
                        String hostname = "/api/request/update";

                        JSONObject object = new JSONObject();

                        try {
                            object.put("inputId", inputId);
                            object.put("userId", session.getId());
                            object.put(Constants.INTENT_PRIORITY, prevPriority[0] + 1);
                            object.put(Constants.INTENT_DELAY, sDelay[prevDelay[0]]);
                            object.put("scheduleDate", scheduleDate.getText().toString());

                            request.post(hostname, object.toString(), "application/json");

                            Toast.makeText(context, "La demande a bien été mise à jour", Toast.LENGTH_LONG).show();

                            Intent intent = new Intent(context, HomeStaffActivity.class);
                            context.startActivity(intent);
                            ((TreatRequestActivity) context).finishAffinity();
                        } catch (JSONException ignored) {
                            Toast.makeText(context, "Impossible de mettre à jour la demande", Toast.LENGTH_LONG).show();
                        }
                    });
                } else if(element.getInt(Constants.INTENT_STATUS) == Constants.REQUEST_RUNNING) {
                    planning.setText(R.string.request_complete);

                    planning.setOnClickListener(view -> {
                        String hostname = "/api/request/complete";

                        JSONObject object = new JSONObject();

                        try {
                            object.put("inputId", inputId);

                            request.post(hostname, object.toString(), "application/json");

                            Toast.makeText(context, "La demande a bien été clôturé", Toast.LENGTH_LONG).show();

                            Intent intent = new Intent(context, HomeStaffActivity.class);
                            context.startActivity(intent);
                            ((TreatRequestActivity) context).finishAffinity();
                        } catch (JSONException ignored) {
                            Toast.makeText(context, "Impossible de clôturer la demande", Toast.LENGTH_LONG).show();
                        }
                    });
                } else
                    planning.setVisibility(View.GONE);

            } catch (Exception e) {
                logger.log(Level.SEVERE, e.getMessage());
            }
        }
    }
}
