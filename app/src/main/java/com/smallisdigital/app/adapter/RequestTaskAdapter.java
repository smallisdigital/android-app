package com.smallisdigital.app.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smallisdigital.app.R;
import com.smallisdigital.app.activity.TreatRequestActivity;
import com.smallisdigital.app.asynctask.JsonArrayAsyncTask;

import org.json.JSONArray;

import org.json.JSONObject;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RequestTaskAdapter extends RecyclerView.Adapter<RequestTaskAdapter.ViewHolder> implements JsonArrayAsyncTask.JSONConsumer {

    private static final Logger logger = Logger.getLogger(RequestTaskAdapter.class.getName());

    private final Context context;

    private JSONArray document = null;

    public RequestTaskAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getItemCount() {
        if(document != null)
            return document.length();

        return 0;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_request_task, parent, false);
        return new ViewHolder(context, view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        try {
            if(document != null) {
                JSONObject item = (JSONObject) document.get(i);
                holder.setElement(item);
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void setJSONDocument(JSONArray object) {
        document = object;

        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final Context context;

        private final TextView inputid;
        private final TextView address;

        private ViewHolder(final Context context, final View itemView) {
            super(itemView);

            this.context = context;

            inputid = itemView.findViewById(R.id.inputid);
            address = itemView.findViewById(R.id.address);

            itemView.setOnClickListener(view -> {
                Intent intent = new Intent(itemView.getContext(), TreatRequestActivity.class);
                intent.putExtra("inputId", inputid.getText().toString().split("°")[1]);
                itemView.getContext().startActivity(intent);
            });
        }

        private void setElement(JSONObject element) {
            try {
                inputid.setText(String.format(Locale.FRENCH, context.getString(R.string.request_inputid), element.getInt("inputId")));
                address.setText(String.format(Locale.FRENCH, context.getString(R.string.request_address), URLDecoder.decode(element.getString("address"), StandardCharsets.UTF_8.toString())));
            } catch (Exception e) {
                logger.log(Level.SEVERE, e.getMessage());
            }
        }
    }
}
