package com.smallisdigital.app.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smallisdigital.app.R;
import com.smallisdigital.app.activity.ModifyProfileActivity;
import com.smallisdigital.app.asynctask.JsonObjectAsyncTask;
import com.smallisdigital.app.common.Constants;

import org.json.JSONObject;

import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class VisualizeProfileAdapter extends RecyclerView.Adapter<VisualizeProfileAdapter.ViewHolder> implements JsonObjectAsyncTask.JSONConsumer {

    private static final Logger logger = Logger.getLogger(VisualizeProfileAdapter.class.getName());

    private final Context context;

    private JSONObject document = null;

    public VisualizeProfileAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_visualize_profile, parent, false);
        return new ViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        try {
            if(document != null)
                holder.setElement(document);

        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void setJSONDocument(JSONObject object) {
        document = object;

        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final Context context;

        private final ImageView image;
        private final ImageView modify;

        private final TextView name;
        private final TextView email;
        private final TextView phone;

        private ViewHolder(final View itemView, final Context context) {
            super(itemView);

            this.context = context;

            image = itemView.findViewById(R.id.image_profile);
            modify = itemView.findViewById(R.id.modify_profile);

            name = itemView.findViewById(R.id.name);
            email = itemView.findViewById(R.id.email);
            phone = itemView.findViewById(R.id.phone);
        }

        private void setElement(JSONObject element) {
            try {
                name.setText(String.format(Locale.FRENCH, context.getString(R.string.user_name), element.getString(Constants.INTENT_FIRSTNAME), element.getString(Constants.INTENT_LASTNAME)));
                email.setText(element.getString(Constants.INTENT_EMAIL));

                if(!element.isNull(Constants.INTENT_PHONE))
                    phone.setText(element.getString(Constants.INTENT_PHONE));
                else
                    phone.setText("");

                modify.setOnClickListener(view -> {
                    Intent intent = new Intent(context, ModifyProfileActivity.class);
                    context.startActivity(intent);
                });
            } catch (Exception e) {
                logger.log(Level.SEVERE, e.getMessage());
            }
        }
    }
}
