package com.smallisdigital.app.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smallisdigital.app.R;
import com.smallisdigital.app.activity.VisualizeRequestZoomActivity;
import com.smallisdigital.app.asynctask.JsonArrayAsyncTask;
import com.smallisdigital.app.common.Constants;

import org.json.JSONArray;

import org.json.JSONObject;

import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class VisualizeRequestAdapter extends RecyclerView.Adapter<VisualizeRequestAdapter.ViewHolder> implements JsonArrayAsyncTask.JSONConsumer {

    private static final Logger logger = Logger.getLogger(VisualizeRequestAdapter.class.getName());

    private final Context context;

    private JSONArray document = null;

    public VisualizeRequestAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getItemCount() {
        if(document != null)
            return document.length();

        return 0;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_visualize_request, parent, false);
        return new ViewHolder(context, view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        try {
            if(document != null) {
                JSONObject item = (JSONObject) document.get(i);
                holder.setElement(item);
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void setJSONDocument(JSONArray object) {
        document = object;

        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final Context context;

        private final TextView inputid;
        private final TextView category;
        private final TextView type;
        private final TextView date;
        private final TextView status;

        private ViewHolder(final Context context, final View itemView) {
            super(itemView);

            this.context = context;

            inputid = itemView.findViewById(R.id.inputid);
            category = itemView.findViewById(R.id.category);
            type = itemView.findViewById(R.id.type);
            date = itemView.findViewById(R.id.date);
            status = itemView.findViewById(R.id.status);

            itemView.setOnClickListener(view -> {
                Intent intent = new Intent(itemView.getContext(), VisualizeRequestZoomActivity.class);
                intent.putExtra("inputId", inputid.getText().toString().split("°")[1]);
                itemView.getContext().startActivity(intent);
            });
        }

        private void setElement(JSONObject element) {
            try {
                inputid.setText(String.format(Locale.FRENCH, context.getString(R.string.request_inputid), element.getInt("inputId")));
                category.setText(String.format(Locale.FRENCH, context.getString(R.string.request_category), element.getString("category")));
                type.setText(String.format(Locale.FRENCH, context.getString(R.string.request_type), element.getString("subcategory")));
                date.setText(String.format(Locale.FRENCH, context.getString(R.string.request_date), element.getString("creationDate")));

                if(element.getInt(Constants.INTENT_STATUS) == Constants.REQUEST_TODO)
                    status.setText(context.getResources().getString(R.string.request_status_todo));
                else if(element.getInt(Constants.INTENT_STATUS) == Constants.REQUEST_RUNNING)
                    status.setText(context.getResources().getString(R.string.request_status_running));
                else
                    status.setText(context.getResources().getString(R.string.request_status_complete));

            } catch (Exception e) {
                logger.log(Level.SEVERE, e.getMessage());
            }
        }
    }
}
