package com.smallisdigital.app.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.smallisdigital.app.R;
import com.smallisdigital.app.activity.VisualizeProfileActivity;
import com.smallisdigital.app.asynctask.HttpRequest;
import com.smallisdigital.app.asynctask.JsonObjectAsyncTask;
import com.smallisdigital.app.common.Constants;
import com.smallisdigital.app.session.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ModifyProfileAdapter extends RecyclerView.Adapter<ModifyProfileAdapter.ViewHolder> implements JsonObjectAsyncTask.JSONConsumer {

    private static final Logger logger = Logger.getLogger(ModifyProfileAdapter.class.getName());

    private final Context context;

    private final SessionManager session;

    private JSONObject document = null;

    public ModifyProfileAdapter(Context context, SessionManager session) {
        this.context = context;
        this.session = session;
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_modify_profile, parent, false);
        return new ViewHolder(view, context, session);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        try {
            if(document != null)
                holder.setElement(document);

        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void setJSONDocument(JSONObject object) {
        document = object;

        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final Context context;
        private final SessionManager session;

        private final HttpRequest request;

        private final TextInputLayout tilFirstname;
        private final TextInputLayout tilLastname;
        private final TextInputLayout tilLevel;
        private final TextInputLayout tilPhone;
        private final TextInputLayout tilBirthday;
        private final TextInputLayout tilAddress;

        private final EditText firstname;
        private final EditText lastname;
        private final EditText level;
        private final EditText email;
        private final EditText phone;
        private final EditText birthday;
        private final EditText address;

        private final Button button;

        private ViewHolder(final View itemView, final Context context, final SessionManager session) {
            super(itemView);

            this.context = context;
            this.session = session;

            request = new HttpRequest(context);

            tilFirstname = itemView.findViewById(R.id.til_firstname);
            tilLastname = itemView.findViewById(R.id.til_lastname);
            tilLevel = itemView.findViewById(R.id.til_level);
            tilPhone = itemView.findViewById(R.id.til_phone);
            tilBirthday = itemView.findViewById(R.id.til_birthday);
            tilAddress = itemView.findViewById(R.id.til_address);

            firstname = itemView.findViewById(R.id.firstname);
            lastname = itemView.findViewById(R.id.lastname);
            level = itemView.findViewById(R.id.level);
            email = itemView.findViewById(R.id.email);
            phone = itemView.findViewById(R.id.phone);
            birthday = itemView.findViewById(R.id.birthday);
            address = itemView.findViewById(R.id.address);

            button = itemView.findViewById(R.id.update);
        }

        private void setElement(JSONObject element) {
            try {
                if(!session.isTaff())
                    tilLevel.setVisibility(View.GONE);

                firstname.setText(element.getString(Constants.INTENT_FIRSTNAME));
                lastname.setText(element.getString(Constants.INTENT_LASTNAME));
                level.setText(element.getString(Constants.INTENT_LEVEL));
                email.setText(element.getString(Constants.INTENT_EMAIL));

                if(!element.isNull(Constants.INTENT_PHONE))
                    phone.setText(element.getString(Constants.INTENT_PHONE));

                if(!element.isNull(Constants.INTENT_BIRTHDAY))
                    birthday.setText(element.getString(Constants.INTENT_BIRTHDAY));

                if(!element.isNull(Constants.INTENT_ADDRESS))
                    address.setText(element.getString(Constants.INTENT_ADDRESS));

                button.setOnClickListener(view -> {
                    String hostname = "/api/user/update";

                    JSONObject object = new JSONObject();

                    try {
                        object.put(Constants.INTENT_FIRSTNAME, firstname.getEditableText().toString().trim());
                        object.put(Constants.INTENT_LASTNAME, lastname.getEditableText().toString().trim());
                        object.put(Constants.INTENT_EMAIL, email.getEditableText().toString().trim());
                        object.put(Constants.INTENT_PHONE, (!phone.getEditableText().toString().isEmpty()) ? phone.getEditableText().toString().trim() : JSONObject.NULL);
                        object.put(Constants.INTENT_BIRTHDAY, (!birthday.getEditableText().toString().isEmpty()) ? birthday.getEditableText().toString().trim() : JSONObject.NULL);
                        object.put(Constants.INTENT_ADDRESS, (!address.getEditableText().toString().isEmpty()) ? address.getEditableText().toString().trim() : JSONObject.NULL);

                        JSONObject result = request.post(hostname, object.toString(), "application/json");

                        /* Suppression des précédents messages d'erreur */
                        tilFirstname.setErrorEnabled(false);
                        tilLastname.setErrorEnabled(false);
                        tilPhone.setErrorEnabled(false);
                        tilBirthday.setErrorEnabled(false);
                        tilAddress.setErrorEnabled(false);

                        if (result != null) {
                            if (result.getBoolean("success")) {
                                Intent intent = new Intent(context, VisualizeProfileActivity.class);
                                context.startActivity(intent);

                                Toast.makeText(context, context.getString(R.string.user_update_info), Toast.LENGTH_LONG).show();
                            } else {
                                JSONArray errors = result.getJSONArray("error");

                                for(int i = 0, l = errors.length() ; i < l ; i++) {
                                    JSONObject error = errors.getJSONObject(i);

                                    switch (error.getString("name")) {
                                        case Constants.INTENT_LASTNAME -> tilLastname.setError(error.getString("value"));
                                        case Constants.INTENT_PHONE -> tilPhone.setError(error.getString("value"));
                                        case Constants.INTENT_BIRTHDAY -> tilBirthday.setError(error.getString("value"));
                                        case Constants.INTENT_ADDRESS -> tilAddress.setError(error.getString("value"));
                                        default -> tilFirstname.setError(error.getString("value"));
                                    }
                                }
                            }
                        }
                    } catch (JSONException ignored) {}
                });
            } catch (Exception e) {
                logger.log(Level.SEVERE, e.getMessage());
            }
        }
    }
}
