package com.smallisdigital.app.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.smallisdigital.app.R;
import com.smallisdigital.app.activity.NoteSurveyActivity;
import com.smallisdigital.app.asynctask.HttpRequest;
import com.smallisdigital.app.asynctask.JsonArrayAsyncTask;
import com.smallisdigital.app.common.Constants;
import com.smallisdigital.app.session.SessionManager;

import org.json.JSONArray;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NoteSurveyAdapter extends RecyclerView.Adapter<NoteSurveyAdapter.ViewHolder> implements JsonArrayAsyncTask.JSONConsumer {

    private static final Logger logger = Logger.getLogger(NoteSurveyAdapter.class.getName());

    private final Context context;

    private final SessionManager session;

    private JSONArray document = null;

    public NoteSurveyAdapter(Context context, SessionManager session) {
        this.context = context;
        this.session = session;
    }

    @Override
    public int getItemCount() {
        if(document != null)
            return document.length();

        return 0;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_note_survey, parent, false);
        return new ViewHolder(context, session, view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        try {
            if(document != null) {
                JSONObject item = (JSONObject) document.get(i);
                holder.setElement(item);
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void setJSONDocument(JSONArray object) {
        document = object;

        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final Context context;

        private final SessionManager session;

        private final TextView name;
        private final TextView question;

        private final RadioGroup radioGroup;

        private final Button button;

        private ViewHolder(final Context context, final SessionManager session, final View itemView) {
            super(itemView);

            this.context = context;

            this.session = session;

            name = itemView.findViewById(R.id.name);
            question = itemView.findViewById(R.id.question);
            radioGroup = itemView.findViewById(R.id.response);
            button = itemView.findViewById(R.id.button);
        }

        private void setElement(JSONObject element) {
            try {
                int surveyId = element.getInt("surveyId");

                name.setText(element.getString("name"));
                question.setText(element.getString("question"));

                JSONArray response = element.getJSONArray("response");

                if(!element.isNull(Constants.INTENT_CONTRIBUTION))
                    button.setVisibility(View.GONE);

                for(int i = 0, l = response.length() ; i < l ; i++) {
                    RadioButton radioButton = new RadioButton(context);
                    radioButton.setText(String.format(Locale.FRENCH, "%s", response.getString(i).split(";", 2)[1]));
                    radioButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, (int) context.getResources().getDimension(R.dimen.visualize_survey_text_size) / context.getResources().getDisplayMetrics().density);
                    radioGroup.addView(radioButton);

                    if(!element.isNull(Constants.INTENT_CONTRIBUTION) && Integer.parseInt(response.getString(i).split(";")[0]) == element.getInt(Constants.INTENT_CONTRIBUTION))
                        radioButton.setChecked(true);

                    if(!element.isNull(Constants.INTENT_CONTRIBUTION))
                        radioButton.setEnabled(false);
                }

                button.setOnClickListener(view -> {
                    String hostname = "/api/survey/vote";

                    HttpRequest request = new HttpRequest(context);
                    JSONObject object = new JSONObject();

                    int idx = radioGroup.indexOfChild(radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()));

                    try {
                        object.put("surveyId", surveyId);
                        object.put("surveyResponseId", response.getString(idx).split(";", 2)[0]);
                        object.put("userId", session.getId());

                        request.post(hostname, object.toString(), "application/json");

                        Toast.makeText(context, "Votre vote a bien été pris en compte", Toast.LENGTH_LONG).show();

                        Intent intent = new Intent(context, NoteSurveyActivity.class);
                        context.startActivity(intent);

                        ((NoteSurveyActivity) context).finish();
                    } catch (JSONException ignored) {
                        Toast.makeText(context, "Impossible de voter pour se sondage", Toast.LENGTH_LONG).show();
                    }
                });

            } catch (Exception e) {
                logger.log(Level.SEVERE, e.getMessage());
            }
        }
    }
}
