package com.smallisdigital.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.smallisdigital.app.R;
import com.smallisdigital.app.dto.Event;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CalendarAdapter extends ArrayAdapter<Object> {

    private final OnAdapterInteractionListener mListener;

    private final Calendar currentDate;
    private final LayoutInflater inflater;
    private final List<Date> dates;
    private final List<Event> events;

    public CalendarAdapter(@NonNull Context context, List<Date> dates, Calendar currentDate, List<Event> events) {
        super(context, R.layout.calendar_single_cell_layout);

        inflater = LayoutInflater.from(context);

        this.currentDate = currentDate;
        this.dates = dates;
        this.events = events;

        if(context instanceof OnAdapterInteractionListener)
            mListener = (OnAdapterInteractionListener) context;
        else
            throw new RuntimeException(context + " must be implement OnAdapterInteractionListener");
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        List<String> inputIds = new ArrayList<>();

        Date monthDate = dates.get(position);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(monthDate);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);

        int currentMonth = currentDate.get(Calendar.MONTH) + 1;
        int currentYear = currentDate.get(Calendar.YEAR);

        View view = convertView;

        if(view == null) {
            view = inflater.inflate(R.layout.calendar_single_cell_layout, parent, false);

            TextView calendarDay = view.findViewById(R.id.calendar_day);

            if (month == currentMonth & year == currentYear)
                calendarDay.setTextColor(Color.parseColor("#000000"));
            else
                calendarDay.setTextColor(Color.parseColor("#CCCCCC"));

            TextView calendarEvent = view.findViewById(R.id.calendar_event);

            calendarDay.setText(String.valueOf(day));

            Calendar event = Calendar.getInstance();

            int counter = 0;

            for(int i = 0 ; i < events.size() ; i++) {
                int nbDay;

                if((nbDay = events.get(i).getDelay() / 86400) == 0)
                    nbDay = 1;

                event.setTime(ConvertStringToDate(events.get(i).getDate()));

                Calendar c1 = Calendar.getInstance();
                c1.setTime(ConvertStringToDate(events.get(i).getDate()));

                Calendar c2 = Calendar.getInstance();
                c2.setTime(ConvertStringToDate(events.get(i).getDate()));
                c2.add(Calendar.DAY_OF_MONTH, nbDay - 1);

                if((calendar.getTime().compareTo(c1.getTime()) >= 0 && calendar.getTime().compareTo(c2.getTime()) <= 0)) {
                    counter++;
                    inputIds.add(events.get(i).getEvent());
                }
            }

            if(counter > 0)
                calendarEvent.setText(String.format(Locale.FRENCH, "%d", counter));
            else
                calendarEvent.setText("");

            view.setOnClickListener(view1 -> {
                if(mListener != null)
                    mListener.onAdapterCreateInteraction(inputIds);
            });
        }

        return view;
    }

    private Date ConvertStringToDate(String eventDate) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.FRANCE);
        Date date = null;

        try {
            date = format.parse(eventDate);
        } catch (ParseException ignored) {
            return null;
        }

        return date;
    }

    @Override
    public int getCount() {
        return dates.size();
    }

    @Override
    public int getPosition(@Nullable Object item) {
        return dates.indexOf((Date) item);
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return dates.get(position);
    }

    public interface OnAdapterInteractionListener {
        void onAdapterCreateInteraction(List<String> inputIds);
    }
}
