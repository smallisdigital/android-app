package com.smallisdigital.app.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smallisdigital.app.R;
import com.smallisdigital.app.activity.TreatRequestActivity;
import com.smallisdigital.app.asynctask.JsonArrayAsyncTask;
import com.smallisdigital.app.common.Constants;

import org.json.JSONArray;

import org.json.JSONObject;

import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PlanningAdapter extends RecyclerView.Adapter<PlanningAdapter.ViewHolder> implements JsonArrayAsyncTask.JSONConsumer {

    private static final Logger logger = Logger.getLogger(PlanningAdapter.class.getName());

    private final Context context;

    private JSONArray document = null;

    public PlanningAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getItemCount() {
        if(document != null)
            return document.length();

        return 0;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_planning, parent, false);
        return new ViewHolder(context, view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        try {
            if(document != null) {
                JSONObject item = (JSONObject) document.get(i);
                holder.setElement(item);
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void setJSONDocument(JSONArray object) {
        document = object;

        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        final Integer[] sDelay = new Integer[] { 1800, 3600, 7200, 14400, 21600, 86400, 172800, 259200, 345600, 432000 };
        final String[] cDelay = new String[] { "30min", "1h", "2h", "4h", "5h", "1j", "2j", "3j", "4j", "5j" };
        private final Context context;

        private final TextView inputId;
        private final TextView scheduleDated;
        private final TextView category;
        private final TextView priority;
        private final TextView delay;

        private ViewHolder(final Context context, final View itemView) {
            super(itemView);

            this.context = context;

            inputId = itemView.findViewById(R.id.inputid);
            scheduleDated = itemView.findViewById(R.id.schedule_date);
            category = itemView.findViewById(R.id.category);
            priority = itemView.findViewById(R.id.priority);
            delay = itemView.findViewById(R.id.delay);

            itemView.setOnClickListener(view -> {
                Intent intent = new Intent(itemView.getContext(), TreatRequestActivity.class);
                intent.putExtra("inputId", inputId.getText().toString().split("°")[1]);
                itemView.getContext().startActivity(intent);
            });
        }

        private void setElement(JSONObject element) {
            try {
                inputId.setText(String.format(Locale.FRENCH, context.getString(R.string.request_inputid_long), element.getString("inputId")));
                scheduleDated.setText(String.format(Locale.FRENCH, context.getString(R.string.request_schedule), element.getString("scheduleDate")));
                category.setText(String.format(Locale.FRENCH, context.getString(R.string.request_category), element.getString("category")));
                priority.setText(String.format(Locale.FRENCH, context.getString(R.string.request_priority), element.getString("priority")));

                for(int i = 0, l = sDelay.length ; i < l ; i++) {
                    if (sDelay[i] == element.getInt(Constants.INTENT_DELAY)) {
                        delay.setText(String.format(Locale.FRENCH, context.getString(R.string.request_delay), cDelay[i]));
                        break;
                    }
                }

            } catch (Exception e) {
                logger.log(Level.SEVERE, e.getMessage());
            }
        }
    }
}
