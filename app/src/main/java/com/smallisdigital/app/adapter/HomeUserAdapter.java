package com.smallisdigital.app.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.smallisdigital.app.R;
import com.smallisdigital.app.activity.VisualizeRequestZoomActivity;
import com.smallisdigital.app.asynctask.JsonArrayAsyncTask;
import com.smallisdigital.app.common.Constants;

import org.json.JSONArray;

import org.json.JSONObject;

import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HomeUserAdapter extends RecyclerView.Adapter<HomeUserAdapter.ViewHolder> implements JsonArrayAsyncTask.JSONConsumer {

    private static final Logger logger = Logger.getLogger(VisualizeRequestAdapter.class.getName());

    private final Context context;

    private JSONArray document = null;

    public HomeUserAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getItemCount() {
        if(document != null)
            return document.length();

        return 0;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_home_user, parent, false);
        return new ViewHolder(context, view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        try {
            if(document != null) {
                JSONObject item = (JSONObject) document.get(i);
                holder.setElement(item);
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void setJSONDocument(JSONArray object) {
        document = object;

        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final Context context;

        private final TextView inputid;
        private final TextView category;
        private final TextView date;
        private final TextView status;
        private final ProgressBar progressBar;
        private final TextView percent;

        private ViewHolder(final Context context, final View itemView) {
            super(itemView);

            this.context = context;

            inputid = itemView.findViewById(R.id.inputid);
            category = itemView.findViewById(R.id.category);
            date = itemView.findViewById(R.id.date);
            status = itemView.findViewById(R.id.status);
            progressBar = itemView.findViewById(R.id.progress);
            percent = itemView.findViewById(R.id.percent);

            itemView.setOnClickListener(view -> {
                Intent intent = new Intent(itemView.getContext(), VisualizeRequestZoomActivity.class);
                intent.putExtra("inputId", inputid.getText().toString().split("°")[1]);
                itemView.getContext().startActivity(intent);
            });
        }

        private void setElement(JSONObject element) {
            try {
                inputid.setText(String.format(Locale.FRENCH, context.getString(R.string.request_inputid), element.getInt("inputId")));
                category.setText(String.format(Locale.FRENCH, context.getString(R.string.request_pre_formated), element.getString("category")));
                date.setText(String.format(Locale.FRENCH, context.getString(R.string.request_pre_formated), element.getString("creationDate")));

                if (element.getInt(Constants.INTENT_STATUS) == Constants.REQUEST_TODO) {
                    status.setText(String.format(Locale.FRENCH, context.getResources().getString(R.string.request_pre_formated), context.getResources().getString(R.string.request_status_todo)));
                    percent.setText(String.format(Locale.FRENCH, context.getString(R.string.request_percent), 25));
                    progressBar.setProgress(1);
                    progressBar.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.progress_color, null)));
                } else if (element.getInt(Constants.INTENT_STATUS) == Constants.REQUEST_RUNNING) {
                    status.setText(String.format(Locale.FRENCH, context.getResources().getString(R.string.request_pre_formated), context.getResources().getString(R.string.request_status_running)));
                    percent.setText(String.format(Locale.FRENCH, context.getString(R.string.request_percent), 50));
                    progressBar.setProgress(2);
                    progressBar.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.progress_color, null)));
                } else if(element.getInt(Constants.INTENT_STATUS) == Constants.REQUEST_COMPLETED && !element.getBoolean("note")) {
                    status.setText(String.format(Locale.FRENCH, context.getResources().getString(R.string.request_pre_formated), context.getResources().getString(R.string.request_status_vote)));
                    percent.setText(String.format(Locale.FRENCH, context.getResources().getString(R.string.request_percent), 75));
                    progressBar.setProgress(3);
                    progressBar.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.progress_color, null)));
                } else {
                    status.setText(String.format(Locale.FRENCH, context.getResources().getString(R.string.request_pre_formated), context.getResources().getString(R.string.request_status_complete)));
                    percent.setText(String.format(Locale.FRENCH, context.getString(R.string.request_percent), 100));
                    progressBar.setProgress(4);
                    progressBar.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.progress_color, null)));
                }

            } catch (Exception e) {
                logger.log(Level.SEVERE, e.getMessage());
            }
        }
    }
}
