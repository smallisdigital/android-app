package com.smallisdigital.app.adapter;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.smallisdigital.app.R;
import com.smallisdigital.app.activity.NoteRequestActivity;
import com.smallisdigital.app.asynctask.JsonObjectAsyncTask;
import com.smallisdigital.app.common.Constants;

import org.json.JSONObject;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class VisualizeRequestZoomAdapter extends RecyclerView.Adapter<VisualizeRequestZoomAdapter.ViewHolder> implements JsonObjectAsyncTask.JSONConsumer {

    private static final Logger logger = Logger.getLogger(VisualizeRequestZoomAdapter.class.getName());

    private final Context context;

    private JSONObject document = null;

    public VisualizeRequestZoomAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_visualize_request_zoom, parent, false);
        return new ViewHolder(context, view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        try {
            if(document != null)
                holder.setElement(document);

        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void setJSONDocument(JSONObject object) {
        document = object;

        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final Context context;

        private final TextView category;
        private final TextView type;
        private final TextView date;
        private final TextView address;
        private final TextView description;

        private final ImageView step1Image;
        private final ImageView step2Image;
        private final ImageView step3Image;
        private final ImageView step4Image;

        private final TextView step1Text;
        private final TextView step2Text;
        private final TextView step3Text;
        private final TextView step4Text;

        private final Button button;

        private ViewHolder(final Context context, final View itemView) {
            super(itemView);

            this.context = context;

            category = itemView.findViewById(R.id.category);
            type = itemView.findViewById(R.id.type);
            date = itemView.findViewById(R.id.date);
            address = itemView.findViewById(R.id.address);
            description = itemView.findViewById(R.id.description);

            step1Image = itemView.findViewById(R.id.step_1_image);
            step2Image = itemView.findViewById(R.id.step_2_image);
            step3Image = itemView.findViewById(R.id.step_3_image);
            step4Image = itemView.findViewById(R.id.step_4_image);

            step1Text = itemView.findViewById(R.id.step_1_text);
            step2Text = itemView.findViewById(R.id.step_2_text);
            step3Text = itemView.findViewById(R.id.step_3_text);
            step4Text = itemView.findViewById(R.id.step_4_text);

            button = itemView.findViewById(R.id.button);
        }

        private void setElement(JSONObject element) {
            try {
                int pxText = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 7f, context.getResources().getDisplayMetrics());
                int pxImage = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32f, context.getResources().getDisplayMetrics());
                String inputId = element.getString("inputId");
                String sAddress = URLDecoder.decode(element.getString("address"), StandardCharsets.UTF_8.toString());
                String sDescription = URLDecoder.decode(element.getString("description"), StandardCharsets.UTF_8.toString());

                category.setText(String.format(Locale.FRENCH, context.getString(R.string.request_category), element.getString("category")));
                type.setText(String.format(Locale.FRENCH, context.getString(R.string.request_type), element.getString("subcategory")));
                date.setText(String.format(Locale.FRENCH, context.getString(R.string.request_date), element.getString("creationDate")));

                step1Image.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.check_green));

                step1Text.setText(String.format(Locale.FRENCH, context.getString(R.string.request_step_1), element.getString("creationDate")));
                step1Text.setBackground(ContextCompat.getDrawable(context, R.drawable.request_step_bg_green));

                if(element.getInt(Constants.INTENT_STATUS) == Constants.REQUEST_TODO) {
                    /* Step 2 */
                    step2Image.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.check_yellow));
                    step2Text.setBackground(ContextCompat.getDrawable(context, R.drawable.request_step_bg_yellow));
                    step2Text.setText(context.getString(R.string.request_step_2a));

                    /* Step 3 */
                    step3Image.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.check_grey));
                    step3Text.setBackground(ContextCompat.getDrawable(context, R.drawable.request_step_bg_grey));

                    /* Step 4 */
                    step4Image.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.check_grey));
                    step4Text.setBackground(ContextCompat.getDrawable(context, R.drawable.request_step_bg_grey));
                } else if(element.getInt(Constants.INTENT_STATUS) == Constants.REQUEST_RUNNING) {
                    /* Step 2 */
                    step2Image.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.check_green));
                    step2Text.setBackground(ContextCompat.getDrawable(context, R.drawable.request_step_bg_green));
                    step2Text.setText(String.format(Locale.FRENCH, context.getString(R.string.request_step_2b), element.getString("startDate")));

                    /* Step 3 */
                    step3Image.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.check_yellow));
                    step3Text.setBackground(ContextCompat.getDrawable(context, R.drawable.request_step_bg_yellow));
                    step3Text.setText(String.format(Locale.FRENCH, context.getString(R.string.request_step_3b), element.getString("scheduleDate")));

                    /* Step 4 */
                    step4Image.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.check_grey));
                    step4Text.setBackground(ContextCompat.getDrawable(context, R.drawable.request_step_bg_grey));
                } else if(element.getInt(Constants.INTENT_STATUS) == Constants.REQUEST_COMPLETED && element.isNull("note")) {
                    /* Step 2 */
                    step2Image.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.check_green));
                    step2Text.setBackground(ContextCompat.getDrawable(context, R.drawable.request_step_bg_green));
                    step2Text.setText(String.format(Locale.FRENCH, context.getString(R.string.request_step_2b), element.getString("startDate")));

                    /* Step 3 */
                    step3Image.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.check_green));
                    step3Text.setBackground(ContextCompat.getDrawable(context, R.drawable.request_step_bg_green));
                    step3Text.setText(String.format(Locale.FRENCH, context.getString(R.string.request_step_3c), element.getString("completeDate")));

                    /* Step 4 */
                    step4Image.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.check_yellow));
                    step4Text.setBackground(ContextCompat.getDrawable(context, R.drawable.request_step_bg_yellow));
                } else {
                    /* Step 2 */
                    step2Image.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.check_green));
                    step2Text.setBackground(ContextCompat.getDrawable(context, R.drawable.request_step_bg_green));
                    step2Text.setText(String.format(Locale.FRENCH, context.getString(R.string.request_step_2b), element.getString("startDate")));

                    /* Step 3 */
                    step3Image.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.check_green));
                    step3Text.setBackground(ContextCompat.getDrawable(context, R.drawable.request_step_bg_green));
                    step3Text.setText(String.format(Locale.FRENCH, context.getString(R.string.request_step_3c), element.getString("completeDate")));

                    /* Step 4 */
                    step4Image.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.check_green));
                    step4Text.setBackground(ContextCompat.getDrawable(context, R.drawable.request_step_bg_green));
                }

                step1Image.getLayoutParams().width = pxImage;
                step1Image.getLayoutParams().height = pxImage;
                step2Image.getLayoutParams().width = pxImage;
                step2Image.getLayoutParams().height = pxImage;
                step3Image.getLayoutParams().width = pxImage;
                step3Image.getLayoutParams().height = pxImage;
                step4Image.getLayoutParams().width = pxImage;
                step4Image.getLayoutParams().height = pxImage;

                step1Text.setPadding(pxText, pxText, pxText, pxText);
                step2Text.setPadding(pxText, pxText, pxText, pxText);
                step3Text.setPadding(pxText, pxText, pxText, pxText);
                step4Text.setPadding(pxText, pxText, pxText, pxText);

                step4Text.setText(context.getString(R.string.request_step_4));

                if(!element.isNull("address"))
                    address.setText(String.format(Locale.FRENCH, context.getString(R.string.request_address), sAddress));
                else
                    address.setText(String.format(Locale.FRENCH, context.getString(R.string.request_address), context.getString(R.string.request_address_ko)));

                if(!element.isNull("description"))
                    description.setText(String.format(Locale.FRENCH, context.getString(R.string.request_description), sDescription));
                else
                    description.setText(String.format(Locale.FRENCH, context.getString(R.string.request_description), context.getString(R.string.request_description_ko)));

                if (!element.isNull("note"))
                    button.setVisibility(View.GONE);
                
                button.setOnClickListener(view -> {
                    try {
                        if (element.getInt(Constants.INTENT_STATUS) != Constants.REQUEST_COMPLETED) {
                            Toast.makeText(context, R.string.request_note_no_completed, Toast.LENGTH_LONG).show();
                        } else if (!element.isNull("note")) {
                            Toast.makeText(context, R.string.request_note_mark, Toast.LENGTH_LONG).show();
                        } else {
                            Intent intent = new Intent(context, NoteRequestActivity.class);
                            intent.putExtra("inputId", inputId);
                            itemView.getContext().startActivity(intent);
                        }
                    } catch (Exception e) {
                        logger.log(Level.SEVERE, e.getMessage());
                    }
                });

            } catch (Exception e) {
                logger.log(Level.SEVERE, e.getMessage());
            }
        }
    }
}
