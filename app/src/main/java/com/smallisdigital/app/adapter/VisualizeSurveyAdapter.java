package com.smallisdigital.app.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smallisdigital.app.R;
import com.smallisdigital.app.activity.VisualizeSurveyActivity;
import com.smallisdigital.app.asynctask.HttpRequest;
import com.smallisdigital.app.asynctask.JsonArrayAsyncTask;

import org.json.JSONArray;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class VisualizeSurveyAdapter extends RecyclerView.Adapter<VisualizeSurveyAdapter.ViewHolder> implements JsonArrayAsyncTask.JSONConsumer {

    private static final Logger logger = Logger.getLogger(VisualizeSurveyAdapter.class.getName());

    private final Context context;

    private JSONArray document = null;

    public VisualizeSurveyAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getItemCount() {
        if(document != null)
            return document.length();

        return 0;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_visualize_survey, parent, false);
        return new ViewHolder(context, view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        try {
            if(document != null) {
                JSONObject item = (JSONObject) document.get(i);
                holder.setElement(item);
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void setJSONDocument(JSONArray object) {
        document = object;

        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final Context context;

        private final TextView name;
        private final TextView question;

        private final LinearLayout linearLayout;

        private final Button button1;
        private final Button button2;

        private ViewHolder(final Context context, final View itemView) {
            super(itemView);

            this.context = context;

            name = itemView.findViewById(R.id.name);
            question = itemView.findViewById(R.id.question);
            linearLayout = itemView.findViewById(R.id.response);
            button1 = itemView.findViewById(R.id.button1);
            button2 = itemView.findViewById(R.id.button2);
        }

        private void setElement(JSONObject element) {
            try {
                int surveyId = element.getInt("surveyId");
                int status = element.getInt("status");

                name.setText(element.getString("name"));
                question.setText(element.getString("question"));

                JSONArray response = element.getJSONArray("response");
                JSONArray vote = element.getJSONArray("vote");

                for(int i = 0, l = response.length() ; i < l ; i++) {
                    TextView textView = new TextView(context);
                    textView.setText(String.format(Locale.FRENCH, "%d%% %s", vote.getInt(i), response.getString(i).split(";", 2)[1]));
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, (int) context.getResources().getDimension(R.dimen.visualize_survey_text_size) / context.getResources().getDisplayMetrics().density);
                    linearLayout.addView(textView);
                }

                switch (status) {
                    case 1:
                        button1.setText(context.getString(R.string.survey_button_close));
                        break;

                    case 2:
                        button1.setText(context.getString(R.string.survey_button_open));
                        break;

                    case 3:
                        button1.setVisibility(View.GONE);
                        button2.setVisibility(View.GONE);
                        break;

                    default:
                        break;
                }

                button1.setOnClickListener(view -> {
                    if(status == 1)
                        updateSurvey(surveyId, 2);
                    else if(status == 2)
                        updateSurvey(surveyId, 1);
                });

                button2.setOnClickListener(view -> updateSurvey(surveyId, 3));

            } catch (Exception e) {
                logger.log(Level.SEVERE, e.getMessage());
            }
        }

        private void updateSurvey(int surveyId, int status) {
            String hostname = "/api/survey/update";

            HttpRequest request = new HttpRequest(context);
            JSONObject object = new JSONObject();

            try {
                object.put("surveyId", surveyId);
                object.put("status", status);

                request.post(hostname, object.toString(), "application/json");

                Toast.makeText(context, "Le sondage a bien été mise à jour", Toast.LENGTH_LONG).show();

                Intent intent = new Intent(context, VisualizeSurveyActivity.class);
                context.startActivity(intent);

                ((VisualizeSurveyActivity) context).finish();
            } catch (JSONException ignored) {
                Toast.makeText(context, "Impossible de mettre à jour le sondage", Toast.LENGTH_LONG).show();
            }
        }
    }
}
