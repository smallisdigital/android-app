package com.smallisdigital.app.util;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class Keyboard {
    private Keyboard() {
        throw new UnsupportedOperationException();
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

        View view = activity.getCurrentFocus();

        if(view == null)
            view = new View(activity);

        if(imm != null)
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}

