package com.smallisdigital.app.util;

import android.app.DatePickerDialog;
import android.content.Context;
import android.text.InputType;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;

import java.util.Calendar;
import java.util.Locale;

public class Dialog {
    private Dialog() {
        throw new UnsupportedOperationException();
    }

    public static void openInfoDialog(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(title);
        builder.setMessage(message);

        builder.show();
    }

    public static void openInputDialog(Context context, String title, StringBuilder response) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        response.setLength(0);

        builder.setTitle(title);

        final EditText input = new EditText(context);

        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("OK", (dialogInterface, i) -> response.append(input.getText().toString()));

        builder.setNegativeButton("Annuler", (dialog, which) -> dialog.cancel());

        builder.show();
    }

    public static void selectDate(Context context, EditText scheduleDate) {

        DatePickerDialog.OnDateSetListener dateSetListener = (view, year, monthOfYear, dayOfMonth) -> scheduleDate.setText(String.format(Locale.FRENCH,"%02d/%02d/%04d", dayOfMonth, monthOfYear + 1, year));

        Calendar calendar = Calendar.getInstance();

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, dateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());

        datePickerDialog.show();
    }
}
