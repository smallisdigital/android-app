package com.smallisdigital.app.util;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PropertiesManager {

    private static final Logger logger = Logger.getLogger(PropertiesManager.class.getName());

    public PropertiesManager() {
        throw new UnsupportedOperationException();
    }

    public static String getProperty(String key, Context context) {
        Properties properties = new Properties();
        AssetManager assetManager = context.getAssets();

        try {
            InputStream inputStream = assetManager.open("application.properties");
            properties.load(inputStream);

            return properties.getProperty(key);
        } catch (IOException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }

        return null;
    }
}
