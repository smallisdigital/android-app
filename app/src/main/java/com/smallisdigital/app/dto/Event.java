package com.smallisdigital.app.dto;

public class Event {
    String event, time, date;
    int delay;

    public Event(String event, String time, String date, int delay) {
        this.event  = event;
        this.time = time;
        this.date = date;
        this.delay = delay;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public int getDelay() {
        return delay;
    }
}
