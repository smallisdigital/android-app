package com.smallisdigital.app.session;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class SessionManager {
    private SharedPreferences prefs = null;
    private SharedPreferences.Editor editor = null;
    private static final String PREFS_NAME = "app_prefs";
    private static final String IS_LOGGED = "isLogged";
    private static final String FIRSTNAME = "firstname";
    private static final String LASTNAME = "lastname";
    private static final String PASSWORD = "password";
    private static final String EMAIL = "email";
    private static final String LEVEL = "level";
    private static final String ID = "id";
    private static final String CATEGORY_ID = "categoryId";
    private static final String CATEGORY_NAME = "categoryName";
    private static final String SUBCATEGORY_ID = "subCategoryId";
    private static final String SUBCATEGORY_NAME = "subCategoryName";

    public SessionManager(Context context) {
        try {
            String masterKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC);

            prefs = EncryptedSharedPreferences.create(
                    PREFS_NAME,
                    masterKeyAlias,
                    context,
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
            );

            editor = prefs.edit();
        } catch (GeneralSecurityException | IOException ignored) {}
    }

    public boolean isLogged() {
        return prefs.getBoolean(IS_LOGGED, false);
    }

    public String getFirstname() {
        return prefs.getString(FIRSTNAME, null);
    }

    public String getLastname() {
        return prefs.getString(LASTNAME, null);
    }

    public String getEmail() {
        return prefs.getString(EMAIL, null);
    }

    public String getPassword() {
        return prefs.getString(PASSWORD, null);
    }

    public int getId() {
        return prefs.getInt(ID, 0);
    }

    public int getLevel() {
        return prefs.getInt(LEVEL, 0);
    }

    public int getCategoryId() {
        return prefs.getInt(CATEGORY_ID, 0);
    }

    public String getCategoryName() {
        return prefs.getString(CATEGORY_NAME, null);
    }

    public int getSubCategoryId() {
        return prefs.getInt(SUBCATEGORY_ID, 0);
    }

    public String getSubcategoryName() {
        return prefs.getString(SUBCATEGORY_NAME, null);
    }

    public boolean isTaff() {
        return getLevel() == 2;
    }

    public void insertUser(int id, String firstname, String lastname, String email, String password, int level, int categoryId, String categoryName, int subCategoryId, String subCategoryName) {
        editor.putBoolean(IS_LOGGED, true);
        editor.putInt(ID, id);
        editor.putString(FIRSTNAME, firstname);
        editor.putString(LASTNAME, lastname);
        editor.putString(EMAIL, email);
        editor.putString(PASSWORD, password);
        editor.putInt(LEVEL, level);
        editor.putInt(CATEGORY_ID, categoryId);
        editor.putString(CATEGORY_NAME, categoryName);
        editor.putInt(SUBCATEGORY_ID, subCategoryId);
        editor.putString(SUBCATEGORY_NAME, subCategoryName);
        editor.commit();
    }

    public void logout() {
        editor.remove(IS_LOGGED).commit();
        editor.remove(ID).commit();
        editor.remove(FIRSTNAME).commit();
        editor.remove(LASTNAME).commit();
        editor.remove(PASSWORD).commit();
        editor.remove(EMAIL).commit();
        editor.remove(LEVEL).commit();
        editor.remove(CATEGORY_NAME).commit();
        editor.remove(SUBCATEGORY_ID).commit();
        editor.remove(SUBCATEGORY_NAME).commit();
    }
}
